//
// Created by lukemartinlogan on 7/22/19.
//

#ifndef ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H
#define ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H

#include <string>
#include <cstring>
#include <ares/c++/ares.h>
#include <common/singleton.h>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/reader.h>

#define ARES_FILTER_CONF Singleton<ConfigurationManager>::GetInstance()

class ConfigurationManager{
public:
    CompressionLibrary compressionLibrary;
    std::string metrics_file;
    float metric_weight[3]={.3,.34,.3};

    ConfigurationManager() :
            compressionLibrary(CompressionLibrary::RICE),
            metrics_file("./metrics.json"){}
};

#endif //ARES_FILTER_PROJECT_CONFIGURATION_MANAGER_H