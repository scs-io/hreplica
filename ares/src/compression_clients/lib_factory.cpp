//
// Created by umashankar on 12/13/17.
//

#include <memory>
#include <ares/c++/compression_clients/lib_factory.h>


//------GET LIBRARY FACTORY (SINGLETON) INSTANCE-----
LibFactory& LibFactory::GetInstance()
{
    static LibFactory s_instance;
    return s_instance;
}

//------GET LIBRARY METHOD------
std::unique_ptr<LibClient> LibFactory::get_library(CompressionLibrary lib_index)
{
    AutoTrace trace = AutoTrace("LibFactory::get_library");
    switch(lib_index){
        case CompressionLibrary::BZ2:
            return std::unique_ptr<BZ2client>(new BZ2client);
        case CompressionLibrary::ZLIB:
            return std::unique_ptr<ZLIBclient>(new ZLIBclient);
        case CompressionLibrary::HUFF:
            return std::unique_ptr<HUFFclient>(new HUFFclient);
        case CompressionLibrary::SF:
            return std::unique_ptr<SFclient>(new SFclient);
        case CompressionLibrary::RICE:
            return std::unique_ptr<RICEclient>(new RICEclient);
        case CompressionLibrary::RLE:
            return std::unique_ptr<RLEclient>(new RLEclient);
        case CompressionLibrary::TRLE:
            return std::unique_ptr<TRLEclient>(new TRLEclient);
        case CompressionLibrary::LZO:
            return std::unique_ptr<LZOclient>(new LZOclient);
        case CompressionLibrary::PITHY:
            return std::unique_ptr<PITHYclient>(new PITHYclient);
        case CompressionLibrary::SNAPPY:
            return std::unique_ptr<SNAPPYclient>(new SNAPPYclient);
        case CompressionLibrary::QLZ:
            return std::unique_ptr<QLZclient>(new QLZclient);
        default: //If the library doesn't exist
            return nullptr;
    }
}

//****************************** EOF *********************************//
