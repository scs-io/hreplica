//
// Created by umashankar on 12/14/17.
//

#include <cstring>
#include <iostream>

#include <ares/c++/compression_clients/huff_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C"{
#include "huffman.h"
}


using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t HUFFclient::est_compressed_size(size_t source_size)
{
    return 0;
}

//-----------HUFFMAN COMPRESSION------------
bool HUFFclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("HUFFclient::compress");
    timer.start();

    //Compress
    if(huffman_encode_memory((const unsigned char*)source, (unsigned int)source_size, (unsigned char**)&destination, (unsigned int*)&destination_size)){
        DBGVAR("Error in Huffman Compression, @ huffman_encode_memory()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------HUFFMAN DECOMPRESSION------------
bool HUFFclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("HUFFclient::decompress");
    timer.start();

    //Decompress
    if(huffman_decode_memory((const unsigned char*)source, (unsigned int)source_size, (unsigned char**)&destination, (unsigned int*)&destination_size))
    {
        DBGVAR("Error in Huffman Decompression, @ huffman_decode_memory()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//
