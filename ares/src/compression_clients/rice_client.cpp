//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares/c++/compression_clients/rice_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "rice.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t RICEclient::est_compressed_size(size_t source_size)
{
    return source_size + 1;
}

//-----------RICE COMPRESSION------------
bool RICEclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("RICEclient::compress");
    timer.start();

    //Compress
    destination_size = (size_t)Rice_Compress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size, RICE_FMT_INT8);
    if(!destination_size){
        DBGVAR("Error in RICE Compression, @ Rice_Compress()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------RICE DECOMPRESSION------------
bool RICEclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in Rice Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("RICEclient::decompress");
    timer.start();

    //Decompress
    Rice_Uncompress((unsigned char*)source, (unsigned char*)destination, (unsigned int)source_size, (unsigned int)destination_size, RICE_FMT_INT8);
    if(!destination_size){
        DBGVAR("Error in RICE Decompression, @ Rice_Uncompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//