//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares/c++/compression_clients/snappy_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C"{
#include "snappy-c.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t SNAPPYclient::est_compressed_size(size_t source_size)
{
    return snappy_max_compressed_length(source_size);
}

//-----------SNAPPY COMPRESSION------------
bool SNAPPYclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("SNAPPYclient::compress");
    timer.start();

    //Compress
    auto status=snappy_compress((const char*)source, source_size, (char*)destination, &destination_size);
    if (status != SNAPPY_OK){
        DBGVAR("Error in SNAPPY Compression, @ snappy_compress()!"+std::to_string(status));
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------SNAPPY DECOMPRESSION------------
bool SNAPPYclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int snappyError = 0;

    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in SNAPPY Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("SNAPPYclient::decompress");
    timer.start();

    //Decompress
    snappyError = snappy_uncompress((const char*)source, source_size, (char*)destination, &destination_size);
    if(snappyError != SNAPPY_OK){
        DBGVAR("Error in SNAPPY Decompression, @ snappy_uncompress()!"+std::to_string(snappyError));
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//