//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares//c++/compression_clients/zlib_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "zlib.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t ZLIBclient::est_compressed_size(size_t source_size)
{
    return compressBound(source_size)+2*1024*1024;
}

//-----------ZLIB COMPRESSION------------
bool ZLIBclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Setting Compression Level:
    int level = Z_BEST_COMPRESSION, ret = 0;

    //---Time measure begins---
    AutoTrace trace = AutoTrace("ZLIBclient::compress");
    timer.start();

    //Compress
    ret = compress2((Bytef*)destination, &destination_size, (const Bytef*)source, source_size, level);
    if(ret != Z_OK){
        DBGVAR("Error in ZLIB Compression, @ compress2()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------ZLIB DECOMPRESSION------------
bool ZLIBclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int ret = 0;

    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in ZLIB Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("ZLIBclient::decompress");
    timer.start();

    //Decompress
    ret = uncompress2((Bytef*)destination, &destination_size, (const Bytef*)source, &source_size);
    if(ret != Z_OK){
        DBGVAR("Error in ZLIB Decompression, @ uncompress2()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//
