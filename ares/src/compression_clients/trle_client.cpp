//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares/c++/compression_clients/trle_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "trle.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t TRLEclient::est_compressed_size(size_t source_size)
{
    return source_size*4/3+1024;
}

//-----------TRLE COMPRESSION------------
bool TRLEclient::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //---Time measure begins---
    AutoTrace trace = AutoTrace("TRLEclient::compress");
    timer.start();

    //Compress
    destination_size = trlec((unsigned char*)source, (unsigned int)source_size, (unsigned char*)destination);
    if(!destination_size){
        DBGVAR("Error in TRLE Compression, @ trlec()!");
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//-----------TRLE DECOMPRESSION------------
bool TRLEclient::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    //Allocate destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in TRLE Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("TRLEclient::decompress");
    timer.start();

    //Decompress
    source_size = trled((unsigned char*)source, (unsigned int)source_size, (unsigned char*)destination, (unsigned int)destination_size);
    if(!source_size){
        DBGVAR("Error in TRLE Decompression, @ trled()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    return SUCCESS;
}

//****************************** EOF *********************************//