//
// Created by umashankar on 12/14/17.
//

#include <iostream>

#include <ares/c++/compression_clients/bz2_client.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

extern "C" {
#include "bzlib.h"
}

using std::cerr;
using std::endl;

//-----------Estimate Compression Buffer---------------
size_t BZ2client::est_compressed_size(size_t source_size)
{
    return source_size + 1024;
}

//-----------BZ2 COMPRESSION------------
bool BZ2client::compress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int bz2error = 0;

    //---Time measure begins---
    AutoTrace trace = AutoTrace("BZ2client::compress");
    timer.start();

    //Compress
    bz2error = BZ2_bzBuffToBuffCompress((char*)destination, (unsigned int*)&destination_size, (char*)source, (unsigned int)source_size, 9, 0 ,0);
    if(BZ_OK != bz2error){
        DBGVAR("Error in BZ2 Compression, @ BZ2_bzBuffToBuffCompress()!"+std::to_string(bz2error));
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();
    return SUCCESS;
}

//-----------BZ2 DECOMPRESSION------------
bool BZ2client::decompress(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size)
{
    int bz2error = 0;

    //Check destination buffer
    destination = malloc(destination_size);
    if(nullptr == destination){
        DBGVAR("Error in BZ2 Decompression, @ malloc()!");
        return FAILURE;
    }

    //---Time measure begins---
    AutoTrace trace = AutoTrace("BZ2client::decompress");
    timer.start();

    //Decompress
    bz2error = BZ2_bzBuffToBuffDecompress((char*)destination, (unsigned int*)&destination_size, (char*)source, (unsigned int)source_size, 1 ,0);
    if(bz2error != BZ_OK){
        DBGVAR("Error in BZ2 Decompression, @ BZ2_bzBuffToBuffDecompress()!");
        free(destination);
        destination = nullptr;
        return FAILURE;
    }

    //---Time measure ends---
    timer.stop();

    //Update data
    return SUCCESS;
}

//****************************** EOF *********************************//
