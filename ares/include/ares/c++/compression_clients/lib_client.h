//
// Created by umashankar on 12/13/17.
//

#ifndef ARES_LIBCLIENT_H
#define ARES_LIBCLIENT_H

#include <iostream>
#include <ares/c++/common/time_counter.h>
#include <ares/c++/common/data_structure.h>
#include <debug.h>
#include <ares/c++/common/error_codes.h>

typedef void*& SOURCE_TYPE;
typedef void*& DESTINATION_TYPE;

class LibClient
{
public:

    LibClient() = default; //Default Ctor
    ~LibClient() = default; //Default Dtor

    //Store uncompressed data
    bool store_uncompressed_data(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size) {
        AutoTrace trace = AutoTrace("LibClient::store_uncompressed_data");
        std::memcpy((char*)destination + AresMetadata::meta_size, source, source_size);
        return SUCCESS;
    }

    //Restore uncompressed data
    bool restore_uncompressed_data(SOURCE_TYPE source, size_t source_size, DESTINATION_TYPE destination, size_t &destination_size) {
        destination = std::malloc(destination_size);
        if(destination == nullptr) {
            DBGVAR("ares_decompress(buffer)->buffer: Can't restore uncompressed data from source!");
            return FAILURE;
        }
        AutoTrace trace = AutoTrace("LibClient::restore_uncompressed_data");
        timer.start();
        std::memcpy(destination, (char*)source, destination_size);
        timer.stop();
        return SUCCESS;
    }

    //Get execution time in milliseconds
    double time_elapsed_msec()
    {
        return timer.get_duration_msec();
    }

    //virtual void init() = 0;
    virtual size_t est_compressed_size(size_t) = 0;
    virtual bool compress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) = 0;
    virtual bool decompress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) = 0;

protected:
    TimeCounter timer;
};

#endif //ARES_LIBCLIENT_H
