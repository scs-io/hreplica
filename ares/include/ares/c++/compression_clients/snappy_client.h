//
// Created by umashankar on 12/14/17.
//

#ifndef ARES_SNAPPYCLIENT_H
#define ARES_SNAPPYCLIENT_H

#include "lib_client.h"

class SNAPPYclient: public LibClient
{
public:
    size_t est_compressed_size(size_t) override;
    bool compress(SOURCE_TYPE, size_t, DESTINATION_TYPE , size_t&) override;
    bool decompress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) override;
};

#endif //ARES_SNAPPYCLIENT_H
