//
// Created by umashankar on 12/14/17.
//

#ifndef ARES_HUFFCLIENT_H
#define ARES_HUFFCLIENT_H

#include "lib_client.h"

class HUFFclient: public LibClient
{
public:
    size_t est_compressed_size(size_t) override;
    bool compress(SOURCE_TYPE, size_t, DESTINATION_TYPE , size_t&) override;
    bool decompress(SOURCE_TYPE, size_t, DESTINATION_TYPE, size_t&) override;
};

#endif //ARES_HUFFCLIENT_H
