//
// Created by umashankar on 4/5/18.
//

#ifndef ARES_ARES_H
#define ARES_ARES_H

#include <string>
#include <ares/c++/common/data_structure.h>

//------------------ Compression Routines -------------------

/* buffer to buffer - 2 default parameters. library choice = Only ares */
bool ares_compress(const void * source, size_t source_size, void *&destination, size_t &destination_size, AresData *data);

/* buffer to file - 3 default parameters */
bool ares_compress(void *&source, size_t source_size, std::string output_file, AresData *data);

/* file/dir to file/dir - 3 default parameters */
bool ares_compress(std::string file_path, AresData *data);



//----------------- Decompression Routines ------------------

/* buffer to buffer */
bool ares_decompress(const void *source, size_t source_size, void *&destination, size_t &destination_size, AresData *data);

/* file to buffer */
bool ares_decompress(std::string file_path, void *&destination, size_t &destination_size, AresData *data);

/* file/dir to file/dir */
bool ares_decompress(std::string file_path, AresData *data);



#endif //ARES_ARES_H
