//
// Created by umashankar on 12/13/17.
//

#ifndef ARES_COMMON_H
#define ARES_COMMON_H


//---------------| USER CHOICES |-----------------

#define NUM_ARES_LIBRARIES 12
#define NUM_ARES_DATA_TYPES 8

enum class CompressionLibrary //"lib_choice" argument in ares_compress()
{
    /* 0    1     2     3    4     5    6    7     8    9     10     11   12 */
    DUMMY, BZ2, ZLIB, HUFF, SF, RICE, RLE, TRLE, LZO, PITHY, SNAPPY, QLZ, DYNAMIC //MAKE DYNAMIC THE LAST OPTION DUE TO ITERATORS!!!
};


enum class DataFormat:int{
    //0     1    2       3      4     5        6    7     8    9        10
    DUMMY, ARES, BINARY, MPIIO, HDF5, NETCDF, CSV, JSON, XML, PARQUET, AVRO
};


/*
enum class PriorityType //"priority" argument in ares_compress()
{
    DUMMY,   //0
    W_1_1_1, //1 - Balanced (for Compression Ratio, Compression Speed and Decompression Speed)
    W_1_0_0, //2 - Compression Speed
    W_0_1_0, //3 - Decompression Speed
    W_0_0_1, //4 - Compression Ratio
    W_1_1_0, //5 - Compression Ratio and Compression Speed
    W_0_1_1, //6 - Compression Ratio and Decompression Speed
    W_1_0_1  //7 - Compression Speed and Decompression Speed
};*/

//---------------| For internal use only |---------------

enum class PathType
{
    NONE, FILE, DIRECTORY
};



#endif //ARES_COMMON_H
