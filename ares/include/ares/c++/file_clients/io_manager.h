//
// Created by umashankar on 3/17/18.
//

#ifndef ARES_IO_MGR_H
#define ARES_IO_MGR_H


#include <string>
#include "ares/c++/common/enumerations.h"



class IOManager{
public:
    IOManager() = default; //Default Ctor
    ~IOManager() = default; //Dtor
    IOManager(const IOManager&) = delete; //No Copy Ctor
    IOManager& operator=(const IOManager&) = delete; //No Assignment Optr
    IOManager(IOManager &&) = delete; // No Move Ctor
    IOManager& operator=(IOManager&&) = delete; //No Move Assignment Optr

    bool write(std::string filename,void*&buffer,size_t &size,DataFormat format,bool op);
    bool read(std::string filename,void*&buffer,size_t &size);
    std::string DeduceFilename(const std::string &file_path, bool op_flag);
private:
    size_t GetFileSize(const std::string &file_path);


};


#endif //ARES_IO_MGR_H
