/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: constants.h
* May 28 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: Defines all constants in the system.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_CONSTANTS_H
#define HERMES_CONSTANTS_H
/***********/
/* Headers */
/***********/
#include <string>
#include <stdint-gcc.h>
#include <boost/interprocess/containers/string.hpp>
#include <common/enumerations.h>

namespace bip=boost::interprocess;
/*************/
/* Constants */
/*************/
/**
 * FIXME: this goes in configuration manager
 */
const EngineType ENGINE_TYPE=EngineType::HDF_MAX_BW; /* defines the Data Placement Engine to be used. (Default HDF_MAX_BW: Maximum Bandwidth)*/
const ReplacementPolicyType CACHE_REPLACEMENT_POLICY=ReplacementPolicyType::LRU; /* Defines which cache replacement policy to be used (Default LRU: Least recently used. */
const int MAX_PREFETCH_EVENTS=1; /* Defines the maximum number of events thats would be prefetched at a given time */
const double MAX_PREFETCH_TIME=10; /* Defines the prefetch window to wait for more events */
const GraphManagerType CURRENT_GRAPH_MANAGER_TYPE=GraphManagerType::HDF5_GRAPH_MANAGER; /* configures the graph Manager type */
const PredictionEngineType CURRENT_PREDICTION_ENGINE_TYPE=PredictionEngineType::HDF5_HISTORY_BASED_PREDICTION_ENGINE; /* configures the prediction engine type */
const DataPlacementEngineType CURRENT_DATA_PLACEMENT_ENGINE_TYPE=DataPlacementEngineType::HDF5_MAX_BW_DATA_PLACEMENT_ENGINE; /* configures the data placement engine for prefetching */
const size_t THREADS_PER_SERVER =4;
const size_t MAX_NUM_FILES = 2048;
const MDMType MDM_TYPE = MDMType::MDM_OPT;
const PredictionMode PREDICTION_MODE=PredictionMode::GRAPH;
const std::string APPLICATION_GRAPH_PATH = "/home/hariharan/CLionProjects/hfetch/test/workflow/graph/";
const std::string APPLICATION_GRAPH_NAME = "h5_buffer_read_fpp_mpi.graph";
const long PREFETCHING_ALGO_DELAY_US = 750000;
const size_t MAX_DIMS = 2;
const double CHUNK_SIZE=1048576.0;

/**
 * These are constants for Hermes
 */
const uint64_t MB=1024*1024; /*Defines Megabyte as a constant*/
const bip::string FILE_PATH_SEPARATOR="/"; /* Defines the file path separator */


#ifdef ENABLE_REPLICATION
const size_t REPLICA_DEFAULT = 3;
const ReplicationRequirement DEFAULT_REPLICATION_REQUIREMENT=ReplicationRequirement::HIGH_READ_BANDWIDTH;
const ReplicationConstraint DEFAULT_REPLICATION_CONSTRAINT=ReplicationConstraint::DUMMY;
#endif
#endif //HERMES_CONSTANTS_H
