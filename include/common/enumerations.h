/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: enumerations.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: Defines enumerations for Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_ENUMERATIONS_H
#define HERMES_ENUMERATIONS_H


#include <rpc/msgpack.hpp>

/**
 * Operation Type enum defines the various operations supported in the Hermes platform.
 */
typedef enum OperationType{
    WRITE=0,
    READ=1
} OperationType;

MSGPACK_ADD_ENUM(OperationType);

/**
 * EngineType defines the various Data Placement Engines present in the Hermes platform.
 */
typedef enum EngineType{
    HDF_MAX_BW=0
} EngineType;
/**
 * InterfaceType defines various interfaces supported by Hermes.
 */
typedef enum InterfaceType{
    HDF5=0
} InterfaceType;
/**
 * ReplacementPolicyType defines various cache replacement policies supported by Hermes.
 */
typedef enum ReplacementPolicyType{
    LRU=0,
    LFU=1
} ReplacementPolicyType;
/**
 * IOClient defines various types of IO Clients supported by Hermes
 */
typedef enum IOClientType{
    HDF5_FILE=0,
    HDF5_MEMORY=1
} IOClientType;

/**
 * MDMType defines various types of MDM supported by Hermes
 */
typedef enum MDMType{
    MDM_FILE_PER_PROCESS=0,
    MDM_OPT=1
} MDMType;

MSGPACK_ADD_ENUM(IOClientType);

/**
 * Prefetch enums
 */

typedef enum EventType{
    OPEN_FILE=0,
    CLOSE_FILE=1,
    OPEN_DATASET=2,
    CLOSE_DATASET=3,
    READ_DATASET=4,
    WRITE_DATASET=5
} EventType;

MSGPACK_ADD_ENUM(EventType);

typedef enum PrefetchStatus{
    PREFETCH_SUCCESS=0,
    PREFETCH_FAILED=1
} PrefetchStatus;

typedef enum GraphManagerType{
    HDF5_GRAPH_MANAGER=0
} GraphManagerType;

typedef enum PredictionEngineType{
    HDF5_HISTORY_BASED_PREDICTION_ENGINE=0
} PredictionEngineType;

typedef enum DataPlacementEngineType{
    HDF5_MAX_BW_DATA_PLACEMENT_ENGINE=0
} DataPlacementEngineType;

typedef enum PredictionMode{
    GRAPH=0,
    PATTERN_PREDICTOR=1
} PredictionMode;

#ifndef ENABLE_COMPRESSION
enum class CompressionLibrary //"lib_choice" argument in ares_compress()
{
    /* 0 */
    DUMMY
    };
#endif

#ifdef ENABLE_REPLICATION
typedef enum class ReplicationMode{
    /* 0 */
    DUMMY=0,
    /* 1 */
    SYNC=1,
    /* 2 */
    ASYNC=2
}ReplicationMode;

typedef enum class ReplicationRequirement{
    /* 0 */
    DUMMY=0,
    /* 1 */
    LOW_READ_LATENCY=1,
    /* 2 */
    HIGH_READ_BANDWIDTH=2,
    /* 3 */
    HIGH_DURABILITY=3,
    /* 4 */
    LOAD_BALANCE=4,
    /* 5 */
    LOW_ENERGY=5
} ReplicationRequirement;

typedef enum class ReplicationConstraint{
    /* 0 */
    DUMMY=0,
    /* 1 */
    LOW_WRITE_LATENCY=1,
    /* 2 */
    HIGH_WRITE_BANDWIDTH=2,
    /* 3 */
    LOW_STORAGE=3
}ReplicationConstraint;
typedef enum class ReplicationHint{
    DEFAULT=0,
    SPECIFIC_COMPRESSION=1,
    NO_COMPRESSION=2,
    RANDOM_PLACEMENT=3
};
#else
typedef enum class ReplicationMode{
    /* 0 */
    DUMMY
}ReplicationMode;
typedef enum class ReplicationRequirement{
    /* 0 */
    DUMMY=0
} ReplicationRequirement;
typedef enum class ReplicationConstraint{
    /* 0 */
    DUMMY=0
}ReplicationConstraint;
#endif

#endif //HERMES_ENUMERATIONS_H
