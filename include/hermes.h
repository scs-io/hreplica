/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hermes.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Buffer API's for Hermes Platform
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HERMES_H
#define HERMES_HERMES_H

#include <hdf5.h>
#include "H5public.h"
#include "H5Rpublic.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Data structure to hold LayerInfo
 */
typedef struct LayerInfo {
    char mount_point_[256];
    float capacity_mb_;
    float bandwidth;
    bool is_memory;
    bool direct_io;
} LayerInfo;


/**
 * This API updates a given layer in Hermes. It has four layers and ID starts
 * from 0 the fastes to 3 the slowest.
 *
 * @param layers
 * @param count
 * @return 0 Success <0 Error
 */
herr_t H5_UpdateLayer(LayerInfo* layers,int count);
/**
 * This API should be called on dataset creation or open.
 * This initializes the buffer for the coming dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param dimentions_
 * @param max_dimentions_
 * @param dataset_id
 * @return 0 Success <0 Error
 */
herr_t H5_BufferInit(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* dimentions_, hsize_t* max_dimentions_, void* dataset_object, hid_t vol_id);
/**
 * This API write data into the buffers for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_end_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferWrite(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* file_start_, hsize_t* file_end_, hsize_t* buf_start_, hsize_t* buf_end_, void* dataset_object, hid_t vol_id, void * buffer);
/**
 * This API reads data from the buffers for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_dim_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferRead(char* filename, char* dataset_name, int rank_, int64_t type_, hsize_t* file_start_, hsize_t* file_end_, hsize_t* buf_start_, hsize_t* buf_dim_, void* dataset_object, hid_t vol_id, void * buffer);
/**
 * This API sync data from the buffers to the PFS for a perticular dataset.
 *
 * @param filename
 * @param dataset_name
 * @param rank_
 * @param type_
 * @param file_start_
 * @param file_end_
 * @param buf_start_
 * @param buf_end_
 * @param dataset_id
 * @param buffer
 * @return  0 Success <0 Error
 */
herr_t H5_BufferSync(char* filename, char* dataset_name, int rank_, hsize_t *dims, hsize_t *max_dims, void* dataset_object, hid_t vol_id);
/**
 * Cleans Up Buffer Libraries data structure
 * @return 0 Success <0 Error
 */
herr_t H5_CleanBuffer();
/**
 * Intercept MPI init calls
 */

int H5_Init();

int H5_Finalize();


#ifdef __cplusplus
}
#endif
#endif //HERMES_HERMES_H
