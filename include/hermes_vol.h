/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hermes_vol_private.c
* June 11 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Hermes VOL Plugin
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HERMES_VOL_H
#define HERMES_PROJECT_HERMES_VOL_H
#include "H5public.h"
#include "H5Rpublic.h"
#include "hermes.h"

#ifdef __cplusplus
extern "C" {
#endif
#define HERMES 555
/**
 * This method implements the setting of vol driver inside application's fapl.
 *
 * @param fapl_id
 * @param layers
 * @param count_
 * @return vol_id
 */
H5_DLL hid_t H5Pset_fapl_hermes_vol(hid_t fapl_id,LayerInfo* layers, uint16_t count_);

#ifdef __cplusplus
}
#endif
#endif //HERMES_PROJECT_HERMES_VOL_H
