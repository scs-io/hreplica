/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: io_client_factory.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Factory for choosing various IO Clients
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_IO_CLIENT_FACTORY_H
#define HERMES_PROJECT_IO_CLIENT_FACTORY_H

#include <common/data_structures.h>
#include <core/interfaces/io_client.h>
#include <core/hdf5_impl/hdf5_client.h>
#include <common/singleton.h>
#include <core/hdf5_impl/hdf5_in_memory.h>
#include <debug.h>

template <typename D,typename I,typename O,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class IOClientFactory {
public:
    IOClientFactory(){
        Singleton<HDF5Client>::GetInstance();
        Singleton<HDF5InMemory>::GetInstance();
    }
    std::shared_ptr<IOClient<D,I,O>> GetClient(IOClientType &type){
        /* Initialize all clients */
        AutoTrace trace = AutoTrace("IOClientFactory::GetClient",type);
        switch(type){
            case IOClientType::HDF5_FILE:{
                return Singleton<HDF5Client>::GetInstance();
            }
            case IOClientType::HDF5_MEMORY:{
                return Singleton<HDF5InMemory>::GetInstance();
            }
        }
    }
};
#endif //HERMES_PROJECT_IO_CLIENT_FACTORY_H
