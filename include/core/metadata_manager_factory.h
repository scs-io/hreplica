//
// Created by hariharan on 2/21/19.
//

#ifndef HERMES_PROJECT_METADATA_MANAGER_FACTORY_H
#define HERMES_PROJECT_METADATA_MANAGER_FACTORY_H

#include <common/data_structures.h>
#include <core/interfaces/metadata_manager.h>
#include <debug.h>
#include <core/hdf5_impl/hdf5_metadata_manager.h>
#include <core/hdf5_impl/hdf5_metadata_manager_opt.h>

template <typename D,typename I,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr>
class MetadataManagerFactory {
public:

    std::shared_ptr<MetadataManager<D,I>> GetMDM(MDMType type){
        AutoTrace trace = AutoTrace("IOClientFactory::GetClient",type);
        switch(type){
            case MDMType::MDM_FILE_PER_PROCESS:{
                return Singleton<HDF5MetadataManager>::GetInstance();
            }
            case MDMType::MDM_OPT:{
                return Singleton<HDF5MetadataManagerOpt>::GetInstance();
            }
        }
    }
};
#endif //HERMES_PROJECT_METADATA_MANAGER_FACTORY_H
