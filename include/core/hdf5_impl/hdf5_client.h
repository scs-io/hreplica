/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_client.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5_CLIENT_H
#define HERMES_HDF5_CLIENT_H


#include <sys/stat.h>
#include <iostream>
#include <common/singleton.h>
#include <core/metadata_manager_factory.h>
#include <core/interfaces/io_client.h>

class HDF5Client: public IOClient<Matrix,HDF5Input,HDF5Output> {
public:
    /**
     * Constructor
     */
    HDF5Client(){} /* default constructor */
    /**
     * Methods
     */

    /**
     * Reads data from source file into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    HDF5Output Read(HDF5Input source,HDF5Input destination) override;

    /**
     * Writes data to a destination file from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    HDF5Output Write(HDF5Input source,HDF5Input destination) override;

    /**
     * Deletes the file specified by input.
     *
     * @param input
     * @return output
     */
    HDF5Output Delete(HDF5Input input) override;

    /**
     * Creates a file using input if it doesnt exists.
     *
     * @param input
     * @return output
     */
    HDF5Output Create(HDF5Input input) override;
    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    uint64_t GetCurrentCapacity(Layer layer) override ;
private:
    inline bool FileExists (const std::string& name) {
        struct stat buffer;
        return (stat (name.c_str(), &buffer) == 0);
    }
    void dataset_get_wrapper(void *dset, hid_t driver_id, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, ...)
    {
        va_list args;
        va_start(args, req);
        H5VLdataset_get(dset, driver_id, get_type, dxpl_id, req, args);
    }
};




#endif //HERMES_HDF5_CLIENT_H
