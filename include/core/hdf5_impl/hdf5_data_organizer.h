/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_data_organizer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of data_organizer on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5DATAORGANIZER_H
#define HERMES_HDF5DATAORGANIZER_H


#include <memory>
#include "../interfaces/data_organizer.h"
#include "../hdf5_impl/hdf5_client.h"
#include "hdf5_metadata_manager.h"
#include <H5public.h>
#include <hdf5.h>
#include "../io_client_factory.h"

class HDF5DataOrganizer: public DataOrganizer<Matrix,HDF5Input,HDF5Output>,public Singleton<HDF5DataOrganizer> {
private:
    typedef IOClientFactory<Matrix,HDF5Input,HDF5Output> HDF5IOFactory;
public:
    HDF5DataOrganizer():DataOrganizer(){} /* default constructor*/
    HDF5DataOrganizer(HDF5DataOrganizer&& other){} /* Move Constructor*/
    HDF5DataOrganizer(HDF5DataOrganizer& other){} /* Copy Constructor*/
    /**
     * Methods
     */

    /**
     * Method moves data from source to destination.
     *
     * @param source
     * @param destination
     * @return output
     */
    HDF5Output Move(HDF5Input source, HDF5Input destination) override;

    /**
     * Methods checks if a layer has capacity to accomodate the given source of data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    bool HasCapacity(HDF5Input source, Layer layer, size_t &used_capacity) override;

    /**
    * Makes capacity required to fit the source in the layer
    *
    * @param source
    * @param layer
    * @return output
    */
    HDF5Output MakeCapacity(HDF5Input source, Layer layer,std::vector<size_t> &used_capacity) override;

    /**
     * Checks if a layer can fit the given data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    bool CanFit(HDF5Input source, Layer layer) override;



    /**
     * Initilizes Buffer
     *
     * @param source
     * @return output
     */
    HDF5Output BufferInit(HDF5Input source) override;

    /**
     * Flushes Buffers for a given source.
     *
     * @param source
     * @return output.
     */
    HDF5Output BufferFlush(HDF5Input source) override;



};


#endif //HERMES_HDF5DATAORGANIZER_H
