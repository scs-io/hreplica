//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef PROJECT_HDF5_PREFETCH_MAX_PERFORMANCE_DPE_H
#define PROJECT_HDF5_PREFETCH_MAX_PERFORMANCE_DPE_H


#include <common/data_structures.h>
#include <core/interfaces/prefetcher/data_placement_engine.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>

class PrefetchHDF5MaxPerformanceDataPlacementEngine: public PrefetchDataPlacementEngine<Matrix,HDF5Input,HDF5Output,HDF5Event> {
private:
    std::vector<Placement<Matrix,HDF5Input, HDF5Output>> CalculatePlacementOptimally(HDF5Input input,Layer layer, const Matrix &buffer_dims);
    float CalculatePlacementScore(HDF5Input input, Layer layer);
    float CalculateMovingScore(HDF5Input input, Layer source_layer, Layer destination_layer);
    Placement<Matrix,HDF5Input, HDF5Output> BuildPlacement(HDF5Input input,Layer layer, bool move_required, bool generate_name,  const Matrix &buffer_dims);
    std::vector<HDF5Input> SplitInput(HDF5Input input, uint64_t capacity_);
public:
    vector<Placement<Matrix,HDF5Input,HDF5Output>> place(HDF5Input input) override;
};


#endif //PROJECT_HDF5_MAX_PERFORMANCE_DPE_H
