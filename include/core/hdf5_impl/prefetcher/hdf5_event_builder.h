//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_HDF5_ACTION_BUILDER_H
#define HERMES_PROJECT_HDF5_ACTION_BUILDER_H


#include <common/data_structures.h>
#include <core/interfaces/prefetcher/event_builder.h>
#include <basket/sequencer/global_sequence.h>
#include <common/constants.h>

class HDF5EventBuilder: public EventBuilder<Matrix,HDF5Input,HDF5Event> {
    basket::global_sequence globalSequence;
    int rank;
public:
    HDF5EventBuilder():globalSequence("EVENT_SEQUENCE",HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE){
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    }
    vector<HDF5Event> build(HDF5Input input, EventType type, uint64_t sequenceId, bool is_end) override;
};


#endif //HERMES_PROJECT_HDF5_ACTION_BUILDER_H
