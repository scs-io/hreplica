/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_in_memory.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of io_client on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_PROJECT_HDF5_MEMORY_H
#define HERMES_PROJECT_HDF5_MEMORY_H


#include <unordered_map>
#include <map>
#include <unordered_set>
#include <common/distributed_ds/hashmap/DistributedHashMap.h>
#include <common/distributed_ds/map/DistributedMap.h>
#include <common/data_structures.h>
#include <core/interfaces/io_client.h>

class HDF5Memory: public IOClient<HDF5Input,HDF5Output> {

    DistributedHashMap<std::string,FileInfo> existing_file;
public:
    /**
     * Constructor
     */
    HDF5Memory():existing_file("EXISTING_FILE_MEMORY"){
    } /* default constructor */
    /**
     * Methods
     */

    /**
     * Reads data from source memory placeholder into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    HDF5Output Read(HDF5Input source, HDF5Input destination) override;

    /**
     * Writes data to a destination memory placeholder from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    HDF5Output Write(HDF5Input input, HDF5Input destination) override;

    /**
     * Deletes the memory placeholder specified by input.
     *
     * @param input
     * @return output
     */
    HDF5Output Delete(HDF5Input input) override;

    /**
     * Creates a memory placeholder using input if it doesnt exists.
     *
     * @param input
     * @return output
     */
    HDF5Output Create(HDF5Input input) override;

    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    int64_t GetCurrentCapacity(Layer layer) override;
private:
    inline bool FileExists (const std::string& name) {
        auto iter=existing_file.Get(name);
        return iter.first;
    }
};


#endif //HERMES_PROJECT_HDF5_IN_MEMORY_H
