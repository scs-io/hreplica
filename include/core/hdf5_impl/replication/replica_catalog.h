//
// Created by hariharan on 5/5/20.
//

#ifndef HERMES_PROJECT_REPLICA_CATALOG_H
#define HERMES_PROJECT_REPLICA_CATALOG_H


#include <common/data_structures.h>
#include <core/metadata_manager_factory.h>
#include <basket/unordered_map/unordered_map.h>
#include <stdlib.h>

class ReplicaCatalog {
private:
#ifdef ENABLE_REPLICATION
    basket::unordered_map<CharStruct,size_t> replica_meta;
    std::shared_ptr<MetadataManager<Matrix,HDF5Input>> metadataManager;
    Location GetMyLocation();
    Location BuildLocation(HDF5Input input);
    double CalculateDistance(Location my_location, HDF5Input source_input);
#endif
public:
#ifdef ENABLE_REPLICATION
    ReplicaCatalog():replica_meta("REPLICA_META", HERMES_CONF->IS_SERVER,HERMES_CONF->MY_SERVER,HERMES_CONF->NUM_SERVERS,HERMES_CONF->SERVER_ON_NODE){
        metadataManager = Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE);
    }
    int Record(HDF5Input& source_original, std::vector<Placement<Matrix,HDF5Input,HDF5Output>>& replica_inputs);
    HDF5Input FindClosest(HDF5Input input);
#else
    ReplicaCatalog(){}
#endif



};


#endif //HERMES_PROJECT_REPLICA_CATALOG_H
