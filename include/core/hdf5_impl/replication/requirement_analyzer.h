//
// Created by hariharan on 5/5/20.
//

#ifndef HERMES_PROJECT_REQUIREMENT_ANALYZER_H
#define HERMES_PROJECT_REQUIREMENT_ANALYZER_H

#include <common/data_structures.h>

class RequirementAnalyzer {
public:
    RequirementAnalyzer(){}

#ifdef ENABLE_REPLICATION
    ReplicationMetrics analyze(HDF5WriteInput &input, ReplicationRequirement requirement, ReplicationConstraint constraint);
#endif
};


#endif //HERMES_PROJECT_REQUIREMENT_ANALYZER_H
