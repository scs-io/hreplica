/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_max_performance_dpe.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the implementation of data_placement_engine on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_HDF5_MAX_PERFORMANCE_DPE_H
#define HERMES_HDF5_MAX_PERFORMANCE_DPE_H


#include <vector>
#include <memory>
#include <core/interfaces/data_placement_engine.h>
#include <core/interfaces/metadata_manager.h>
#include <core/interfaces/data_organizer.h>
#include <core/hdf5_impl/hdf5_metadata_manager.h>
#include <core/hdf5_impl/hdf5_data_organizer.h>
#include <math.h>
#include <stdint-gcc.h>

#ifdef ENABLE_REPLICATION
#include <core/hdf5_impl/replication/replica_catalog.h>
#endif

class HDF5MaxPerformanceDataPlacementEngine: public DataPlacementEngine<Matrix,HDF5ReadInput,HDF5WriteInput,HDF5Input,HDF5Output> {
public:
    /**
     * Constructors
     */
    HDF5MaxPerformanceDataPlacementEngine():DataPlacementEngine(){} /* default constructor*/
    HDF5MaxPerformanceDataPlacementEngine(HDF5MaxPerformanceDataPlacementEngine&& other){} /* Move Constructor*/
    HDF5MaxPerformanceDataPlacementEngine(HDF5MaxPerformanceDataPlacementEngine& other){} /* Copy Constructor*/

    /**
     * Methods
     */

    /**
     * Identifies placement based on policy for data to be written.
     *
     * @param input
     * @return vector of placements
     */
    std::vector<Placement<Matrix,HDF5Input,HDF5Output>> PlaceWriteData(HDF5WriteInput input) override;

    /**
     * Identifies where data is placed in DMSH.
     *
     * @param input
     * @return vector of placements
     */
    std::vector<Placement<Matrix,HDF5Input,HDF5Output>> PlaceReadData(HDF5ReadInput input) override;

    /**
     * Dynamic Programming recursive method to place data into layers Optimally.
     * Algorithm: minimize time as score will maximize Performance.
     * <ol>
     *      <li>if HasCapacity then calculate_placement_score()</li>
     *      <li>else if Can fit then
     *              min(
     *                  move data to next layer + calculate_placement_score(),
     *                  CalculatePlacementOptimally(input,layer+1)
     *              )
     *      </li>
     *      <li>else
     *              c = get capacity of layer
     *              input_part_1=c
     *              input_part_2=input.size-c
     *              min(
     *                  move data(c) to next layer + calculate_placement_score(input_part_1) +  CalculatePlacementOptimally(input_part_2,layer+1),
     *                  CalculatePlacementOptimally(input,layer+1)
     *              )     *
     *      </li>
     * </ol>
     * @param input
     * @param layer
     * @param buffer_dims
     * @return vector of placements
     */
    std::vector<Placement<Matrix,HDF5Input,HDF5Output>> CalculatePlacementOptimally(HDF5WriteInput input,Layer layer,
                                                                             Matrix &buffer_dims,
                                                                             std::vector<size_t> &used_capacity,
                                                                             CharStruct &destination_filename,
                                                                             bool is_replica);

    /**
     * Maps HDF5 dataset type to an integer.
     * */

    int
    get_dataset_type(hid_t type);

    /**
    * Calculates the score for placing compressed input on layer
    *
    * @param input
    * @param layer
    * @return score value in float
    */
    float CalculatePlacementScore(HDF5Input input,Layer layer, CompressionLibrary comp_lib);

    std::pair<float,CompressionLibrary> FindOptimalCompression(HDF5WriteInput input, Layer layer);


    /**
     * Calculates the cost of moving the data to next layer.
     *
     * @param input
     * @param source_layer
     * @param destination_layer
     * @return score value in float
     */
    float CalculateMovingScore(HDF5Input input,Layer source_layer,Layer destination_layer, size_t &used_capacity);

    /**
     * Builds Placement Object for given input on a layer.
     *
     * @param input
     * @param layer
     * @param move_required
     * @param comp_lib
     * @param generate_name
     * @param buffer_dims
     * @return Placement Object
     */
    Placement<Matrix,HDF5Input, HDF5Output> BuildPlacement(HDF5Input input,
                                                    Layer layer,
                                                    float score,
                                                    bool move_required,
                                                    CompressionLibrary comp_lib,
                                                    CharStruct &destination_filename,
                                                    Matrix &buffer_dims);

    /**
     * Splits the inputs into multiple chunks based on the capacity provided
     *
     * @param input
     * @param capacity_
     * @return vector of inputs.
     */
    std::vector<HDF5Input> SplitInput(HDF5Input input, uint64_t capacity_);
#ifdef ENABLE_REPLICATION
    std::vector<Placement<Matrix,HDF5Input, HDF5Output>> CalculateReplicaPlacement(HDF5WriteInput input, Layer layer, Matrix &buffer_dims, vector<size_t> &used_capacity, CharStruct &destination_filename);
#endif
};


#endif //HERMES_HDF5_MAX_PERFORMANCE_DPE_H
