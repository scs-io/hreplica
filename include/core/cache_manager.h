/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: cache_manager.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Cache Manager Class for Hermer. This class uses a
* cache replacement policy that are defined in constants.h by
* CACHE_REPLACEMENT_POLICY
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_CACHE_MANAGER_H
#define HERMES_CACHE_MANAGER_H


#include <memory>
#include <iostream>
#include <common/data_structures.h>
#include <core/interfaces/replacement_policy.h>
#include <common/constants.h>
#include <core/replacement_policy/lru_policy.h>
#include <common/singleton.h>
#include <debug.h>

template <typename I,typename D,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type* = nullptr>
class CacheManager {
private:

public:
    /**
     * constructor
     */
    CacheManager(){
    }
    /**
     * Methods
     */
    /**
     * Inserts a cache-line for corresponding data and Input.
     *
     * @param data
     * @param input
     * @return status code 0 if successful < 0 if it fails
     */
    int Insert(D data, I input){
        AutoTrace trace = AutoTrace("CacheManager::Insert",data,input);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Insert(data, input);
    }
    /**
     * Evicts a cache-line for corresponding data and Input from layer.
     * @param layer
     * @return evicted pair of Data and Input
     */
    std::pair<D,I> Evict(Layer layer){
        AutoTrace trace = AutoTrace("CacheManager::Evict",layer);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Evict(layer);
    }
    /**
     * deletes a cache-line for Data
     * @param data
     * @return deleted pair of Data and Input
     */
    std::pair<D,I> Delete(D data){
        AutoTrace trace = AutoTrace("CacheManager::Delete",data);
        return Singleton<LRUPolicy<I,D>>::GetInstance()->Delete(data);
    }

};




#endif //HERMES_CACHE_MANAGER_H
