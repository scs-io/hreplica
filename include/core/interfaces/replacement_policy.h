/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: replacement_policy.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Cache Replacement Policies defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_REPLACEMENT_POLICY_H
#define HERMES_REPLACEMENT_POLICY_H

#include <common/data_structures.h>

template <typename I,typename D,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type* = nullptr>
class ReplacementPolicy{
public:
    /**
     * Virtual Methods
     */
    /**
    * Inserts a new data and input into cache-line.
    *
    * @param data
    * @param input
    * @return status of code. success 0 failure < 0
    */
    virtual int Insert(D data, I input) = 0;

    /**
    * Evicts LRU data from layer.
    *
    * @param layer
    * @return evicted pair of data and input.
    */
    virtual std::pair<D, I> Evict(Layer layer) = 0;

    /**
    * Deletes cache-line for a given Data.
    *
    * @param data
    * @return deleted pair of data and Input
    */
    virtual std::pair<D, I> Delete(D data) = 0;

};
#endif //HERMES_REPLACEMENT_POLICY_H
