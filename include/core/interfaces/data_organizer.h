/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: data_organizer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Data Organizers defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/
#ifndef HERMES_DATA_ORGANIZER_H
#define HERMES_DATA_ORGANIZER_H


#include <common/data_structures.h>
template <typename D,typename I, typename O,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class DataOrganizer {
public:
    /**
     * Virtual Methods
     */

    /**
     * Method moves data from source to destination.
     *
     * @param source
     * @param destination
     * @return output
     */
    virtual O Move(I source, I destination)=0;

    /**
     * Methods checks if a layer has capacity to accomodate the given source of data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    virtual bool HasCapacity(I source, Layer layer, size_t &used_capacity)=0;

    /**
     * Makes capacity required to fit the source in the layer
     *
     * @param source
     * @param layer
     * @return output
     */
    virtual O MakeCapacity(I source, Layer layer,std::vector<size_t> &used_capacity)=0;

    /**
     * Checks if a layer can fit the given data.
     *
     * @param source
     * @param layer
     * @return true if layer can fit source else false.
     */
    virtual bool CanFit(I source, Layer layer)=0;

    /**
     * Initilizes Buffer
     *
     * @param source
     * @return output
     */
    virtual O BufferInit(I source)=0;

    /**
     * Flushes Buffers for a given source.
     *
     * @param source
     * @return output.
     */
    virtual O BufferFlush(I source)=0;


};


#endif //HERMES_DATA_ORGANIZER_H
