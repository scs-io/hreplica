/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: io_client.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all IO Clients defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_IO_CLIENT_H
#define HERMES_IO_CLIENT_H

#include <common/data_structures.h>
template <typename D,typename I,typename O,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class IOClient {
public:
    /**
     * Virtual Methods
     */
     /**
      * Creates a file using input if it doesnt exists.
      *
      * @param input
      * @return output
      */
    virtual O Create(I input)=0;

    /**
     * Reads data from source file into destination (memory)
     *
     * @param source
     * @param destination
     * @return read_output
     */
    virtual O Read(I source, I destination)=0;

    /**
     * Writes data to a destination file from source (memory)
     *
     * @param input
     * @param destination
     * @return write_output
     */
    virtual O Write(I input, I destination)=0;

    /**
     * Deletes the file specified by input.
     *
     * @param input
     * @return output
     */
    virtual O Delete(I input)=0;

    /*
     * TODO:
     *
     * Input compress_input(I input)
     * returns compressed input with type = string and new size.
     * Input decompress_input(I input)
     * */

    /**
     * Get Capacity of the current layer
     *
     * @param layer
     * @return
     */
    virtual uint64_t GetCurrentCapacity(Layer layer) = 0;
};


#endif //HERMES_IO_CLIENT_H
