/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: data_placement_engine.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Data Placement Engines defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/
#ifndef HERMES_DATA_PLACEMENT_ENGINE_H
#define HERMES_DATA_PLACEMENT_ENGINE_H


#include <vector>
#include <common/data_structures.h>



template <typename D,typename IR,typename IW, typename I,typename O,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<ReadInput<D>, IR>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<WriteInput<D>, IW>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr>
class DataPlacementEngine {
public:
    /**
     * Virtual Methods
     */

    /**
     * Identifies placement based on policy for data to be written.
     *
     * @param input
     * @return vector of placements
     */
    virtual std::vector<Placement<D,I,O>> PlaceWriteData(IW input)=0;

    /**
     * Identifies where data is placed in DMSH.
     *
     * @param input
     * @return vector of placements
     */
    virtual std::vector<Placement<D,I,O>> PlaceReadData(IR input)=0;
};


#endif //HERMES_DATA_PLACEMENT_ENGINE_H
