//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_PREFETCH_DATA_PLACEMENT_ENGINE_H
#define HERMES_PROJECT_PREFETCH_DATA_PLACEMENT_ENGINE_H

#include <common/data_structures.h>

template <typename D,typename I,typename O, typename E,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of <Event <D,I>, E>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class PrefetchDataPlacementEngine{
public:
    virtual vector<Placement<D,I,O>> place(I input) = 0;
};
#endif //HERMES_PROJECT_DATA_PLACEMENT_ENGINE_H
