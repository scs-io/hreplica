//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_ACTION_BUILDER_H
#define HERMES_PROJECT_ACTION_BUILDER_H

#include <common/data_structures.h>

template <typename D,typename I,typename E,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type* = nullptr>
class EventBuilder{
private:
public:
    virtual std::vector<E> build(I input,EventType type,uint64_t sequenceId, bool is_end) = 0;
};
#endif //HERMES_PROJECT_ACTION_BUILDER_H
