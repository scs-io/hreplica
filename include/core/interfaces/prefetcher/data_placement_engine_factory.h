//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_DATA_PLACEMENT_ENGINE_FACTORY_H
#define HERMES_PROJECT_DATA_PLACEMENT_ENGINE_FACTORY_H
#include <core/interfaces/prefetcher/data_placement_engine.h>
#include <core/hdf5_impl/prefetcher/hdf5_max_performance_dpe.h>
template <typename D,typename I,typename O, typename E
        , typename std::enable_if<std::is_base_of < Event<D,I>, E>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr
        , typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr
        , typename std::enable_if<std::is_base_of<Output, O>::value>::type* = nullptr>
class DataPlacementEngineFactory{
public:
    std::shared_ptr<PrefetchDataPlacementEngine<D,I,O,E>> getDataPlacementEngine(DataPlacementEngineType type){
        switch(type){
            case DataPlacementEngineType::HDF5_MAX_BW_DATA_PLACEMENT_ENGINE:{
                return Singleton<PrefetchHDF5MaxPerformanceDataPlacementEngine>::GetInstance();
            }
            default:{
                break;
            }
        }
    }
};
#endif //HERMES_PROJECT_DATA_PLACEMENT_ENGINE_FACTORY_H
