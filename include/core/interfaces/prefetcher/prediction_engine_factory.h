//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_PREDICTION_ENGINE_FACTORY_H
#define HERMES_PROJECT_PREDICTION_ENGINE_FACTORY_H

#include <core/hdf5_impl/prefetcher/hdf5_history_based_prediction_engine.h>
#include <common/enumerations.h>

template <typename D,typename I,typename E,typename G,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<GraphNode<D,I>, G>::value>::type* = nullptr>
class PredictionEngineFactory{
public:
    std::shared_ptr<PredictionEngine<D,I,E,G>> getPredictionEngine(PredictionEngineType type){
        switch(type){
            case PredictionEngineType::HDF5_HISTORY_BASED_PREDICTION_ENGINE:{
                return Singleton<HDF5HistoryBasedPredictionEngine>::GetInstance();
            }
            default:{
                break;
            }
        }
    }
};
#endif //HERMES_PROJECT_PREDICTION_ENGINE_FACTORY_H
