//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_ACCESS_PATTERN_PREDICTOR_H
#define HERMES_PROJECT_ACCESS_PATTERN_PREDICTOR_H

#include <common/data_structures.h>
template <typename D,typename I,typename E,typename G,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<GraphNode<D,I>, G>::value>::type* = nullptr>
class PredictionEngine{
public:
    virtual void load() = 0;
    virtual void enhance(vector<E> events) = 0;
    virtual vector<pair<t_mili,E>> getNextTriggers() = 0;
    virtual void store() = 0;
};
#endif //HERMES_PROJECT_ACCESS_PATTERN_PREDICTOR_H
