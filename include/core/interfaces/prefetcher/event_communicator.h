//
// Created by HariharanDevarajan on 2/7/2019.
//

#ifndef HERMES_PROJECT_ACTION_COMMUNICATOR_H
#define HERMES_PROJECT_ACTION_COMMUNICATOR_H

#include <common/data_structures.h>
#include <basket/queue/queue.h>


template <typename D,typename I,typename E,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type* = nullptr>
class EventCommunicator{
private:
    size_t default_server;
    int rank;
    basket::queue<E> queue;
public:
    EventCommunicator():queue("PREFTECH_QUEUE",HERMES_CONF->RANKS_PER_SERVER){
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        default_server=(size_t)rank/HERMES_CONF->RANKS_PER_SERVER;
    }
    PrefetchStatus publish(vector<E> events, uint32_t server=-1){
        if(server==-1) server=default_server;
        bool status=true;
        for(E event:events){
            status = status && queue.Push(event,server);
        }
        return PrefetchStatus::PREFETCH_SUCCESS;

    }

    vector<E> subscribe(uint32_t server=-1){
        if(server==-1) server=default_server;
        bool status=true;
        vector<E> events=vector<E>();
        auto result = queue.Pop(server);
        if(result.first){
                events.push_back(result.second);
        }
        return events;
    }
};


#endif //HERMES_PROJECT_ACTION_COMMUNICATOR_H
