//
// Created by HariharanDevarajan on 2/15/2019.
//

#ifndef HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H
#define HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H

#include <core/hdf5_impl/prefetcher/hdf5_graph_manager.h>
#include <common/data_structures.h>
#include <common/enumerations.h>
#include <common/singleton.h>
#include <core/interfaces/prefetcher/graph_manager.h>

template <typename D,typename I,typename E,typename G,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<GraphNode<D,I>, G>::value>::type* = nullptr>
class GraphManagerFactory{
public:
std::shared_ptr<GraphManager<D,I,E,G>> getGraphManager(GraphManagerType type){
    switch(type){
        case GraphManagerType::HDF5_GRAPH_MANAGER:{
            return Singleton<HDF5GraphManager>::GetInstance();
        }
        default:{
            break;
        }
    }
}
};
#endif //HERMES_PROJECT_GRAPH_MANAGER_FACTORY_H
