/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: metadata_manager.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the interface for all Metadata Managers defined
* in Hermes.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_METADATA_MANAGER_H
#define HERMES_METADATA_MANAGER_H

#include <functional>
#include <vector>
#include <string>
#include <common/data_structures.h>
#include <common/configuration_manager.h>






template<typename D, typename I,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type * = nullptr>
class MetadataManager {
private:

public:

    /**
     * Initialize Hermes selection engine.
     *
     * [type_id][comp_lib_id][0] = [c1]
     * [type_id][comp_lib_id][1] = [c2]
     * [type_id][comp_lib_id][2] = [c3]
     */

    MetadataManager() {
    }





    /**
     * Virtual Methods
     */
    /**
     * This methods updates the metadata on initialization (i.e. Open or Create)
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnInit(I source) = 0;

    /**
     * This methods updates the metadata on read
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnRead(I source, I destination) = 0;

    /**
     * This methods updates the metadata on write
     *
     * @param source
     * @param destination
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnWrite(I source, I destination) = 0;

    /**
     * This methods updates the metadata on Sync of data
     *
     * @param source
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateOnSync(I source) = 0;

    /**
     * This method finds where the given data is buffered in the DMSH
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    virtual std::vector<std::pair<I, I>> FindBufferedData(I source) = 0;

    /**
     * This method fetches all the data is buffered in the DMSH.
     *
     * @param source
     * @return vector of sources and destination of buffered data
     */
    virtual std::vector<std::pair<I, I>> GetAllBufferedData(I source) = 0;

    /**
     * This method fetches which data should be evicted to make more space (remaining_capacity) in the layer.
     *
     * @param source
     * @param layer
     * @param remaining_capacity
     * @return vector of destination of buffered data
     */
    virtual std::vector<I> GetBufferedDataToEvict(I source, Layer layer, float remaining_capacity,std::vector<size_t> &used_capacity) = 0;

    /**
     * Updates the buffered data's layer location. Typically used when we move data between layers.
     *
     * @param source
     * @param layer
     * @return status of operation success 0, failure < 0
     */
    virtual int UpdateLayer(I source, Layer layer) = 0;

    /**
     * Generates a unique filename for the data.
     *
     * @return filename of buffered data
     */
    virtual CharStruct GenerateBufferFilename(I input) = 0;

    /**
    * Check if file has some pieces left
    *
    * @param buf_source
    * @return True or False
    */
    virtual bool HasChunk(I buf_source, Layer layer) = 0;
};


#endif //HERMES_METADATA_MANAGER_H
