/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: lru_policy.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines LRU (Least Recently Used) Policy for Cache replacement.
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_LRU_POLICY_H
#define HERMES_LRU_POLICY_H


#include <memory>
#include <unordered_map>
#include <sys/time.h>
#include <map>
#include <tgmath.h>
#include <stdint-gcc.h>
#include <core/interfaces/replacement_policy.h>
#include <common/singleton.h>
#include <debug.h>

template <typename I,typename D,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type* = nullptr,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type* = nullptr>
class LRUPolicy: public ReplacementPolicy<I,D> {
private:
    /**
     * Attributes
     */
    std::unordered_map<uint8_t,std::map<long int,std::pair<D,I>>> cache_line; /* maintains cache-line of layer -> {timestamp, data}*/
    std::unordered_map<D,std::pair<uint8_t,long int>> data_map; /* maintains map of data to a perticular layer and timestamp for search. */

public:
    /**
     * Constructors
     */
    LRUPolicy():cache_line(),data_map(),ReplacementPolicy<I,D>(){} /* default constructor. */
    /**
     * Methods
     */
    /**
     * Inserts a new data and input into cache-line.
     *
     * @param data
     * @param input
     * @return status of code. success 0 failure < 0
     */
    int Insert(D data, I input) override{
        AutoTrace trace = AutoTrace("LRUPolicy::Insert",data,input);
        /* Calculate timestamp in micro seconds. */
        struct timeval tp;
        gettimeofday(&tp, NULL);
        long int us = tp.tv_sec * 1000000 + tp.tv_usec;
        /* create new entries. */
        std::pair<D,I> data_pair=std::make_pair(data,input);
        std::pair<long int,std::pair<D,I>> time_data_pair =
                std::make_pair(us,data_pair);
        std::pair<long int,uint8_t> time_layer_pair =
                std::make_pair(us,input.layer.id_);
        /*insert or replace new entries*/
        auto cache_line_map_iterator=cache_line.find(input.layer.id_);
        if(cache_line_map_iterator == cache_line.end()){
            std::map<long int,std::pair<D,I>> time_map=std::map<long int,std::pair<D,I>>();
            time_map.insert(time_data_pair);
            cache_line.insert(std::make_pair(input.layer.id_,time_map));
        }else{
            cache_line_map_iterator->second.insert(time_data_pair);
        }
        data_map.insert(std::make_pair(data,time_layer_pair));
        return 0;
    };

    /**
     * Deletes cache-line for a given Data.
     *
     * @param data
     * @return deleted pair of data and Input
     */
    std::pair<D, I> Delete(D data) override{
        AutoTrace trace = AutoTrace("LRUPolicy::Delete",data);
        /* find data from data map. */
        auto iter=data_map.find(data);
        if(iter!=data_map.end()){
            /* use datamap value to find cache-line. */
            auto cache_line_map_iterator=cache_line.find(iter->second.second);
            if(cache_line_map_iterator!=cache_line.end()){
                auto policy_iter=cache_line_map_iterator->second.find(iter->second.first);
                if(policy_iter!=cache_line_map_iterator->second.end()){
                    /* delete cacheline entry and datamap entry. */
                    std::pair<D, I> pair=policy_iter->second;
                    cache_line_map_iterator->second.erase(policy_iter);
                    data_map.erase(iter);
                    return pair;
                }
            }

        }
        return std::pair<D,I>();
    };

    /**
     * Evicts LRU data from layer.
     *
     * @param layer
     * @return evicted pair of data and input.
     */
    std::pair<D, I> Evict(Layer layer) override{
        AutoTrace trace = AutoTrace("LRUPolicy::Evict",layer);
        /* find layer from cacheline. */
        auto cache_line_map_iterator=cache_line.find(layer.id_);
        if(cache_line_map_iterator!=cache_line.end()){
            /* get the least recently used data. */
            auto policy_iter=cache_line_map_iterator->second.begin();
            if(policy_iter!=cache_line_map_iterator->second.end()){
                /* delete the least recently used data. */
                std::pair<D, I> pair=std::make_pair(policy_iter->second.first,policy_iter->second.second);
                auto iter=data_map.find(policy_iter->second.first);
                if(iter!=data_map.end()){
                    data_map.erase(iter);
                }
                cache_line_map_iterator->second.erase(policy_iter);
                return pair;
            }
            return std::pair<D,I>();
        }
        return std::pair<D,I>();
    };

};


#endif //HERMES_LRU_POLICY_H
