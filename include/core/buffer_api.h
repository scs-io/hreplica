/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: buffer.h
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Defines the Buffer API's for Hermes Platform
*
*-------------------------------------------------------------------------
*/

#ifndef HERMES_BUFFER_API_H
#define HERMES_BUFFER_API_H


#include <memory>
#include <core/hdf5_impl/hdf5_metadata_manager_opt.h>

#ifdef ENABLE_COMPRESSION

#include <ares_filter/hdf5_ares_filter.h>

#endif

#ifdef ENABLE_REPLICATION
#include <include/core/hdf5_impl/replication/requirement_analyzer.h>
#include <core/hdf5_impl/replication/replica_catalog.h>
#endif

#include "common/enumerations.h"
#include "interfaces/data_placement_engine.h"
#include "interfaces/io_client.h"
#include "hdf5_impl/hdf5_data_organizer.h"
#include "hdf5_impl/hdf5_client.h"
#include "hdf5_impl/hdf5_max_performance_dpe.h"
#include "hdf5_impl/hdf5_metadata_manager.h"
#include "common/constants.h"
#include "io_client_factory.h"
#include <debug.h>
#include "prefetch.h"

template<typename D,typename I, typename O, typename IW, typename OW, typename IR, typename OR, typename E, typename G,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<ReadInput<D>, IR>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<ReadOutput, OR>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<WriteInput<D>, IW>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<WriteOutput, OW>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Event<D,I>, E>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<GraphNode<D,I>, G>::value>::type * = nullptr>
class BufferAPI {
private:
    typedef IOClientFactory<D,I, O> IOFactory;
    bool isInitialized;
    /**
     * Attributes
     */
    std::shared_ptr<DataPlacementEngine<D,IR, IW, I, O>> data_placement_engine; /* instance of data placement engine*/
    std::shared_ptr<DataOrganizer<D,I, O>> data_organizer;
    std::shared_ptr<MetadataManager<D,I>> metadataManager;
    std::shared_ptr<IOFactory> io_factory;
    std::shared_ptr<Prefetch<D,I, O, E, G>> prefetcher;
public:
    /**
     * constructors
     */
    BufferAPI() : isInitialized(false) {}

    /**
     * Methods
     */
    void Init(InterfaceType interface_type) {
        if (!isInitialized) {
            switch (interface_type) {
                /* when interface used is HDF5 */
                case InterfaceType::HDF5: {
                    /* make shared object of HDF5MaxPerformanceDataPlacementEngine */
                    data_placement_engine = Singleton<HDF5MaxPerformanceDataPlacementEngine>::GetInstance();
                    data_organizer = Singleton<HDF5DataOrganizer>::GetInstance(); /* initialise Buffer*/
                    metadataManager = Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE); /* initialize metadata*/
                    io_factory = Singleton<IOFactory>::GetInstance();/* Initialize Buffers*/
#ifdef ENABLE_PREFETCH
                    prefetcher=Singleton<Prefetch<I,O,E,G>>::GetInstance();
                    prefetcher->Init(interface_type);
                    int rank;
                    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
                    if ((rank + 1) % HERMES_CONF->RANKS_PER_SERVER == 0) {
                        prefetcher->runServer(1);
                    }
#endif
#ifdef ENABLE_COMPRESSION
                    /*Initialize HDF5 compression libraries*/
                    H5_init_ares_filters();
#endif
                }
                    break;
            }
        }
        isInitialized = true;
    }

/**
 * Initializes the Buffer Platform for a given file and dataset.<br>
 * This function should be called on creation/opening of a dataset.
 *
 * @param input
 * @return output
 */
    inline O BufferInit(I input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferInit", input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::OPEN_DATASET);
#endif
        data_organizer->BufferInit(input); /* initialise Buffer*/
        metadataManager->UpdateOnInit(input); /* initialize metadata*/
        O output = io_factory->GetClient(input.layer.io_client_type)->Create(input); /* create a placeholder at PFS */
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::OPEN_DATASET,sequenceId,true);
#endif
        return output;
    }

    /**
     * This method buffers a write operation into DMHS.<br>
     * This function should be called for buffering write of dataset into DMSH.
     * @param input
     * @return write_output
     */
    inline O BufferWrite(IW input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferWrite", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::WRITE_DATASET);
#endif
#ifdef ENABLE_REPLICATION
        auto analyzer = Singleton<RequirementAnalyzer>::GetInstance();
        analyzer->analyze(input,DEFAULT_REPLICATION_REQUIREMENT, DEFAULT_REPLICATION_CONSTRAINT);
        auto replicas_placement_map = std::unordered_map<Matrix, std::vector<Placement<D,I,O>>>();
#endif
        /* use DPE to decide where to place write data. */
        std::vector<Placement<D,I, O>> placements = data_placement_engine->PlaceWriteData(input);
        OW main_output;
        auto used_capacity = std::vector<size_t>();
        for(auto layer = Layer::FIRST; layer!= nullptr; layer=layer->next){
            used_capacity.push_back(0);
        }
        for (Placement<D,I, O> placement:placements) {
            if (placement.output.CheckSuccess()) {
                if (placement.destination_info.layer == *Layer::LAST) {
                    printf("Data written into %s\n", placement.destination_info.layer.layer_loc.c_str());
                }
                /* make space if we dont have space in current layer. */

                if (placement.move_required_ ||
                    !data_organizer->HasCapacity(placement.destination_info, placement.destination_info.layer,used_capacity[placement.destination_info.layer.id_])) {
                    data_organizer->MakeCapacity(placement.destination_info, placement.destination_info.layer,used_capacity);
                }
                /* write data into current layer. */
                O output = io_factory->GetClient(placement.destination_info.layer.io_client_type)->Write(
                        placement.source_info, placement.destination_info);
                if (!output.CheckSuccess()) return output;
                /* update metadata after successful write operation. */
#ifdef ENABLE_REPLICATION
                if(placement.destination_info.is_replica){
                    auto iter = replicas_placement_map.find(placement.source_info.GetData());
                    std::vector<Placement<D,I,O>> replica_placements;
                    if(iter != replicas_placement_map.end()){
                        replica_placements = iter->second;
                    }else{
                        replica_placements = std::vector<Placement<D,I,O>>();
                    }
                    replica_placements.push_back(placement);
                    replicas_placement_map.insert_or_assign(placement.source_info.GetData(),replica_placements);
                }else
#endif
                    metadataManager->UpdateOnWrite(placement.user_info, placement.destination_info);

            } else main_output = placement.output;
        }
#ifdef ENABLE_REPLICATION
        for(auto replica_placement_iter: replicas_placement_map){
            Singleton<ReplicaCatalog>::GetInstance()->Record(input, replica_placement_iter.second);
        }
#endif
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::WRITE_DATASET,sequenceId,true);
#endif
        return main_output;
    };

    inline O BufferRead(IR input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferRead", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::READ_DATASET);
#endif

#ifdef ENABLE_REPLICATION
        auto closest_data = Singleton<ReplicaCatalog>::GetInstance()->FindClosest(input);
#else
        auto closest_data=input;
#endif
        /* fetch where to read data from DPE. */
        std::vector<Placement<D, I, O>> placements = data_placement_engine->PlaceReadData(closest_data);
        O main_output;
        if (placements.size() == 0) main_output.error = -1;
        for (Placement<D, I, O> placement:placements) {
            if (placement.output.CheckSuccess()) {
                if (placement.destination_info.layer == *Layer::LAST) {
                    printf("Data read from %s\n", placement.destination_info.layer.layer_loc.c_str());
                }
                placement.destination_info.HDF5Input::buffer = input.HDF5Input::buffer;
                /* read data from source. */
                O output = io_factory->GetClient(placement.destination_info.layer.io_client_type)->Read(
                        placement.source_info, placement.destination_info);
                /* udpate metadata after successful read. */
                metadataManager->UpdateOnRead(placement.user_info, placement.destination_info);
            } else main_output = placement.output;
        }
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::READ_DATASET,sequenceId,true);
#endif
        return main_output;
    };

    inline O BufferSync(I input) {
        AutoTrace trace = AutoTrace("BufferAPI::BufferSync", 1, input);
        uint64_t sequenceId;
#ifdef ENABLE_PREFETCH
        sequenceId = prefetcher->prefetch(input,EventType::CLOSE_DATASET);
#endif
        /* Call Buffer flush over dataset described by input. */
        O output;
        //output = data_organizer->BufferFlush(input);
        MPI_Barrier(MPI_COMM_WORLD);
#ifdef ENABLE_PREFETCH
        prefetcher->prefetch(input,EventType::CLOSE_DATASET,sequenceId,true);
#endif
        return output;

    }

    inline void Finalize() {
        AutoTrace trace = AutoTrace("BufferAPI::Finalize", 0);
        MPI_Barrier(MPI_COMM_WORLD);
        /* Call Buffer flush over dataset described by input. */
#ifdef ENABLE_PREFETCH
        prefetcher->stopServer();
#endif
    }
};

#endif //HERMES_BUFFER_API_H
