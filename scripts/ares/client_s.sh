#!/bin/bash
NODES=$(cat compute_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
echo "Stopping client on $node"
ssh $node /bin/bash << EOF
sudo kill-pvfs2-client
mount | grep pvfs2
EOF
done

