#!/bin/bash
NODES=$(cat burst_buffer_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
ssh $node /bin/bash << EOF
ps -aef | grep pvfs2
EOF
done

