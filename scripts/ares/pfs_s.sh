#!/bin/bash
NODES=$(cat pfs_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
ssh $node /bin/bash << EOF
rm -rf /mnt/hdd/hdevarajan/storage/*
killall pvfs2-server
ps -aef | grep pvfs2
EOF
done

