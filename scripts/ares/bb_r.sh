#!/bin/bash
NODES=$(cat burst_buffer_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
ssh $node /bin/bash << EOF
rm -rf /mnt/ssd/hdevarajan/storage/*
killall pvfs2-server
mkdir -p /mnt/ssd/hdevarajan/storage
pvfs2-server -f -a ${node} ${SCRIPT_DIR}/conf/bb.conf
pvfs2-server -a ${node} ${SCRIPT_DIR}/conf/bb.conf
ps -aef | grep pvfs2
EOF
done

