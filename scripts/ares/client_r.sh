#!/bin/bash
NODES=$(cat compute_nodes)
SCRIPT_DIR=`pwd`
for node in $NODES
do
echo "Starting client on $node"
ssh $node /bin/bash << EOF
sudo kill-pvfs2-client
mkdir -p /mnt/nvme/hdevarajan/pfs /mnt/nvme/hdevarajan/bb
sudo insmod ${ORANGEFS_KO}/pvfs2.ko
sudo ${ORANGEFS_PATH}/sbin/pvfs2-client -p ${ORANGEFS_PATH}/sbin/pvfs2-client-core
sudo mount -t pvfs2 tcp://ares-stor-01-40g:3334/orangefs /mnt/nvme/hdevarajan/pfs
sudo mount -t pvfs2 tcp://ares-stor-26-40g:3334/orangefs /mnt/nvme/hdevarajan/bb
mount | grep pvfs2
EOF
done

