#!/bin/bash -l

#SBATCH -q regular 
#SBATCH -N 32
#SBATCH -t 02:00:00
#SBATCH -J hermes_1024_procs
#SBATCH -C haswell
#SBATCH --ntasks-per-node=64
#SBATCH --ntasks=2048
#SBATCH -c 1 
#DW jobdw capacity=2048GB access_mode=striped type=scratch
cd ${SCRATCH}/hermes/hermes/build
rm -rf /global/homes/l/lrknox/bb
ln -s ${DW_JOB_STRIPED} ~/bb
export HDF5_USE_FILE_LOCKING=FALSE
export XTPE_LINK_TYPE=dynamic
export CRAYPE_LINK_TYPE=dynamic
export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64_lin/:/global/cscratch1/sd/lrknox/hermes/hdf5/hdf5/lib:/opt/cray/job/2.2.2-6.0.5.0_8.47__g3c644b5.ari/lib64:/usr/syscom/nsg/lib 
##srun "/global/cscratch1/sd/lrknox/hermes/hermes/build/test/h5_buffer_write_fpp_mpi" "-d" "0" "-i" "1" "-f" "/dev/shm/temp" "-n" "1" "-l" "1#256_80000_1_/dev/shm/temp_0"
##ctest -R  cori_128_h5_buffer_write_fpp_mpi_1_1_RAM_0_MPI_128
ctest -R cori_128_h5_buffer_write_fpp_mpi_
cp  Testing/Temporary/LastTest.log  ../logs/2048_${SLURM_JOB_ID}.log
