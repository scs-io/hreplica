
#####DISCLAIMER####
#RUN set_environment.sh first!!!!!
#Make sure to modify the necessary variables
#This script is automatic!!!

######SET DEFAULT VARIABLES#####

BLD=$HCOMPRESS_DEP_BLD
SRC=$HCOMPRESS_SRC_SRC/comp_libs
cd $SRC

#########BCL#########
wget -O bcl.tar.bz2 https://sourceforge.net/projects/bcl/files/bcl/bcl-1.2.0/bcl-1.2.0.tar.bz2/download
tar -xvjf bcl.tar.bz2

#########BROTLI#########
git clone https://github.com/google/brotli

#########BZIP2#########
wget -O bzip2.tar.gz https://sourceforge.net/projects/bzip2/files/bzip2-1.0.6.tar.gz/download
tar -xvzf bzip2.tar.gz

#########HUFFMAN#########
wget -O huffman.tgz https://sourceforge.net/projects/huffman/files/huffman/1.2/huffman-1.2.tgz/download
tar -xvzf huffman.tgz

#########BSC#########
git clone https://github.com/IlyaGrebnov/libbsc.git

#########LZ4#########
git clone https://github.com/lz4/lz4.git

#########LZMA1900#########

#########LZO#########
wget -O lzo.tar.gz http://www.oberhumer.com/opensource/lzo/download/lzo-2.10.tar.gz
tar -xvzf lzo.tar.gz

#########PITHY#########
git clone https://github.com/johnezang/pithy.git

#########QUICKLZ#########
git clone https://github.com/robottwo/quicklz.git

#########SNAPPY#########
git clone https://github.com/google/snappy.git

#########TurboRLE#########
git clone https://github.com/powturbo/TurboRLE.git 

#########ZLIB#########
git clone https://github.com/madler/zlib.git

######INSTALL THEM ALL!!!
cd $SRC
cp $HCOMPRESS_SRC/docs/CMakeLists.ares.txt $SRC/CMakeLists.txt
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$BLD ../
make && make install


