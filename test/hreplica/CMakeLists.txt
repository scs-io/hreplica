cmake_minimum_required(VERSION 3.10.1)
project(hreplica-test)
set(INSTALL_DIR $ENV{INSTALL_DIR})
message(INFO ${INSTALL_DIR})
include_directories(${INSTALL_DIR}/include/)

add_subdirectory(bdcats)
add_subdirectory(vpic)

set(CMAKE_CXX_STANDARD 17)
set(LIBS -L${INSTALL_DIR}/lib64 -L${CMAKE_BINARY_DIR}/ares/datasets  -ldatasets -L${INSTALL_DIR}/lib -lhdf5 -lhdf5_hl -lm -lrt -L${INSTALL_DIR}/lib -lmpi -lpthread -lboost_filesystem -ldl ${CMAKE_BINARY_DIR}/external/rpclib/librpc.so)
include_directories(${CMAKE_SOURCE_DIR}/ares/libs/zlib-1.2.11)
include_directories(${CMAKE_SOURCE_DIR})

set(CTEST_ENVIRONMENT "LD_LIBRARY_PATH=${CMAKE_BINARY_DIR}/")
set($ENV{LD_PRELOAD} "$ENV{LD_PRELOAD} ${CMAKE_BINARY_DIR}")
message(INFO " ${CMAKE_C_FLAGS}")
#######COPY CONFIGURATION FILES#####
#file(COPY ${CMAKE_SOURCE_DIR}/conf/configuration.json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
#file(COPY ${CMAKE_SOURCE_DIR}/conf/metrics.json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})


#######EXECUTABLE CREATION############

set(test_files_mpi h5_buffer_write_fpp_mpi h5_buffer_write_shared_mpi)
set(test_files_gcc h5_buffer_write ${test_files_mpi})
set(test_files ${test_files_gcc})

#Create set of executables
foreach (test_file ${test_files})
    foreach (hermes_case ${hermes_cases})
        set(exec ${test_file}_${hermes_case})
        add_executable(${exec} ${test_file}.c util.h timer.h)
        add_dependencies(${exec} ${hermes_case})
        target_include_directories(${exec} PRIVATE "${CMAKE_BINARY_DIR}/")
        target_link_libraries(${exec} ${LIBS})
        target_link_libraries(${exec} ${hermes_case})
        set_target_properties(${exec} PROPERTIES FOLDER test/hcompress)
    endforeach ()
endforeach ()

#######TEST CREATION############

#Create GCC test for a particular executable
function(gcc_test   location net_size distribution_name exec testid args)
    set(metrics_name metrics_${distribution_name}.json)
    set(config_name ${location}_config${net_size}.json)
    set(test_parameters ${args} -g ${CMAKE_SOURCE_DIR}/conf/data/instances/${location}/${config_name} -m ${CMAKE_SOURCE_DIR}/conf/data/instances/${metrics_name})
    set(test_name ${testid})
    add_test(NAME ${test_name} COMMAND ${CMAKE_BINARY_DIR}/test/hcompress/${exec} ${test_parameters})
    set_tests_properties(${test_name} PROPERTIES WILL_FAIL true)
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "LD_PRELOAD=${CMAKE_BINARY_DIR}/ares/datasets/libdatasets.so")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_PLUGIN_PATH=$ENV{HDF5_PLUGIN_PATH}")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_VOL_CONNECTOR=${hermes_case}")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "RUN_DIR=$ENV{RUN_DIR}")
endfunction()

#Create MPI test for a particular executable
function(mpi_test   location net_size distribution_name mpiprocs exec testid args)
    set(metrics_name metrics_${distribution_name}.json)
    set(config_name ${location}_config${net_size}.json)
    set(test_parameters -n ${mpiprocs} ${CMAKE_BINARY_DIR}/test/hcompress/${exec} ${args} -g ${CMAKE_SOURCE_DIR}/conf/data/instances/${location}/${config_name} -m ${CMAKE_SOURCE_DIR}/conf/data/instances/${metrics_name})
    set(test_name ${testid})
    add_test(NAME ${test_name} COMMAND mpirun ${test_parameters})
    set_tests_properties(${test_name} PROPERTIES WILL_FAIL true)
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "LD_PRELOAD=${CMAKE_BINARY_DIR}/ares/datasets/libdatasets.so")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_PLUGIN_PATH=$ENV{HDF5_PLUGIN_PATH}")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_VOL_CONNECTOR=${hermes_case}")
endfunction()

#Create a sequence of unique tests with different parameters
function(create_gcc_tests     location nreqs io_sizes distributions distribution_names)
    foreach (test_file ${test_files_gcc})
        foreach (hermes_case ${hermes_cases})
            foreach(io_size ${io_sizes})
                list(LENGTH distribution_names dist_list_length)
                MATH(EXPR max_dist_idx "${dist_list_length} - 1")
                foreach(dist_idx RANGE ${max_dist_idx})
                    list(GET distributions ${dist_idx} distribution)
                    list(GET distribution_names ${dist_idx} distribution_name)
                    set(exec ${test_file}_${hermes_case})
                    set(testid ${location}_anatomy_gcc_${test_file}_${io_size}_${nreqs}_${distribution_name}_${hermes_case}_end)
                    set(args -n ${nreqs} -i ${io_size} -p ${distribution})
                    math(EXPR net_size ${io_size}*${nreqs})
                    gcc_test(${location} ${net_size} ${distribution_name} ${exec} ${testid} "${args}")
                endforeach()
            endforeach()
        endforeach ()
    endforeach()
endfunction()

function(create_mpi_tests     location nreqs io_sizes mpiprocs_list ranks_per_server_list distributions distribution_names)
    foreach (test_file ${test_files_mpi})
        foreach (hermes_case ${hermes_cases})
            foreach(io_size ${io_sizes})
                foreach(mpiprocs ${mpiprocs_list})
                    foreach(ranks_per_server ${ranks_per_server_list})
                        list(LENGTH distribution_names dist_list_length)
                        MATH(EXPR max_dist_idx "${dist_list_length} - 1")
                        foreach(dist_idx RANGE ${max_dist_idx})
                            list(GET distributions ${dist_idx} distribution)
                            list(GET distribution_names ${dist_idx} distribution_name)
                            set(exec ${test_file}_${hermes_case})
                            set(testid ${location}_anatomy_mpi_${test_file}_${mpiprocs}_${ranks_per_server}_${io_size}_${nreqs}_${distribution_name}_${hermes_case}_end)
                            set(args -n ${nreqs} -i ${io_size} -r ${ranks_per_server}  -p ${distribution})
                            math(EXPR net_size ${io_size}*${nreqs})
                            mpi_test(${location} ${net_size} ${distribution_name} ${mpiprocs} ${exec} ${testid} "${args}")
                        endforeach()
                    endforeach()
                endforeach()
            endforeach()
        endforeach ()
    endforeach()
endfunction()

set(base_distribution "0#100#4")
set(base_distribution_name normal)
set(distribution_names normal gamma exponential uniform)
set(distributions "0#100#4" "1#2#25" "2#.1" "3#0#50")

#Create local test cases
set(CONFIG_PATH ${CMAKE_SOURCE_DIR}/conf/data/local)
set(io_sizes 1 4 16)
create_gcc_tests(local 100 "${io_sizes}" "${base_distribution}" "${base_distribution_name}")
set(io_sizes 1)
create_gcc_tests(local 500 "${io_sizes}" "${distributions}" "${distribution_names}")
set(ranks_per_server 1 2 4 8)
set(mpiprocs_list 8)
create_mpi_tests(local 100 1 "${mpiprocs_list}" "${ranks_per_server}" "${base_distribution}" "${base_distribution_name}")

#Create cluster test cases
set(CONFIG_PATH ${CMAKE_SOURCE_DIR}/conf/data/cluster)
set(io_sizes 1 4 16 64 128)
create_gcc_tests(cluster 8000 "${io_sizes}" "${base_distribution}" "${base_distribution_name}")
set(ranks_per_server 5 10 20 40)
set(mpiprocs_list 320 640 1280 2560)
create_mpi_tests(cluster 1000 1 "${mpiprocs_list}" "${ranks_per_server}" "${base_distribution}" "${base_distribution_name}")



# Motivation Cases
#-n 8 /home/hariharan/CLionProjects/hreplica/build/test/hreplica/h5_buffer_write_fpp_mpi_hermes_replica_ares -i 1 -n 32 -p 0#100#4 -c 1 -m /home/hariharan/CLionProjects/hreplica/conf/data/instances/metrics_normal.json -g

set(layer_config /home/hariharan/CLionProjects/hreplica/conf/configuration.json)
set(compression_metrics /home/hariharan/CLionProjects/hreplica/conf/data/instances/metrics_normal.json)
set(plugin_path /home/hariharan/CLionProjects/hreplica/build)

set(general_args -g ${layer_config} -p "0#100#4" -m ${compression_metrics})
set(num_processes 1 2 4 8)
set(compression_libs 1 2 3 4 5 6 7 8 9 10 11 12)
set(debugs 0 1)

foreach (debug ${debugs})
# baseline
foreach (proc ${num_processes})
    set(test_parameters -n ${proc} ${CMAKE_BINARY_DIR}/test/hreplica/h5_buffer_write_fpp_mpi_hermes -i 1 -n 32 ${general_args} -z ${debug})
    set(test_name ${debug}_motivation_baseline_${proc})
    add_test(NAME ${test_name} COMMAND mpirun ${test_parameters})
    set_tests_properties(${test_name} PROPERTIES WILL_FAIL false)
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "LD_PRELOAD=${CMAKE_BINARY_DIR}/ares/datasets/libdatasets.so ${CMAKE_BINARY_DIR}/basket/libbasket.so")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_PLUGIN_PATH=${plugin_path}")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_VOL_CONNECTOR=hermes")
endforeach()
# replica without compression
foreach (proc ${num_processes})
    set(test_parameters -n ${proc} ${CMAKE_BINARY_DIR}/test/hreplica/h5_buffer_write_fpp_mpi_hermes_replica -i 1 -n 32 ${general_args})
    set(test_name ${debug}_motivation_replica_${proc})
    add_test(NAME ${test_name} COMMAND mpirun ${test_parameters})
    set_tests_properties(${test_name} PROPERTIES WILL_FAIL false)
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "LD_PRELOAD=${CMAKE_BINARY_DIR}/ares/datasets/libdatasets.so ${CMAKE_BINARY_DIR}/basket/libbasket.so")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_PLUGIN_PATH=${plugin_path}")
    set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_VOL_CONNECTOR=hermes")
endforeach()

# replica with single_compression
foreach (proc ${num_processes})
    foreach (c_lib ${compression_libs})
        set(test_parameters -n ${proc} ${CMAKE_BINARY_DIR}/test/hreplica/h5_buffer_write_fpp_mpi_hermes_replica_ares -c ${c_lib} -i 1 -n 32 ${general_args})
        set(test_name ${debug}_motivation_replica_ares_${c_lib}_${proc})
        add_test(NAME ${test_name} COMMAND mpirun ${test_parameters})
        set_tests_properties(${test_name} PROPERTIES WILL_FAIL false)
        set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "LD_PRELOAD=${CMAKE_BINARY_DIR}/ares/datasets/libdatasets.so ${CMAKE_BINARY_DIR}/basket/libbasket.so")
        set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_PLUGIN_PATH=${plugin_path}")
        set_property(TEST ${test_name} APPEND PROPERTY ENVIRONMENT "HDF5_VOL_CONNECTOR=hermes")
    endforeach()
endforeach()

endforeach()