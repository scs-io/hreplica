#include <malloc.h>
#include <memory.h>
#include <hdf5.h>
#include <assert.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <src/core/hdf5_impl/prefetcher/hdf5_history_based_prediction_engine.h>
/* #include <src/core/prefetch.h> */
#include <src/common/data_structures.h>
#include <src/common/enumerations.h>
#include <hermes.h>

int main(int argc, char** argv) {
    char* homepath = getenv("RUN_DIR");
    LayerInfo layers[4];
    sprintf(layers[0].mount_point_,"%s/ramfs/",homepath);
    layers[0].capacity_mb_=1024;
    layers[0].bandwidth=8000;
    layers[0].is_memory=false;
    sprintf(layers[1].mount_point_,"%s/nvme/",homepath);
    layers[1].capacity_mb_=2*1024;
    layers[1].bandwidth=2000;
    layers[1].is_memory=false;
    sprintf(layers[2].mount_point_,"%s/bb/",homepath);
    layers[2].capacity_mb_=4*1024;
    layers[2].bandwidth=400;
    layers[2].is_memory=false;
    sprintf(layers[3].mount_point_,"%s/pfs/",homepath);
    layers[3].capacity_mb_=8*1024;
    layers[3].bandwidth=100;
    layers[3].is_memory=false;
    // H5_UpdateLayer()
    Layer* current_layer=NULL;
    Layer* previous_layer=NULL;
    int count = 4;
    for(int order=0;order<count;order++){
        current_layer=new Layer();
        if(order==0){
            Layer::FIRST=current_layer;
        }
        current_layer->id_=order+1;
        current_layer->capacity_mb_=layers[order].capacity_mb_;
        current_layer->io_client_type=layers[order].is_memory?IOClientType::HDF5_MEMORY:IOClientType::HDF5_FILE;
        current_layer->direct_io=layers[order].direct_io;
        current_layer->bandwidth_mbps_=layers[order].bandwidth;
        current_layer->layer_loc=std::string(layers[order].mount_point_);
        current_layer->previous=previous_layer == NULL?nullptr:previous_layer;
        current_layer->next= nullptr;
        if(previous_layer != NULL){
            previous_layer->next=current_layer;
        }
        previous_layer=current_layer;
    }
    Layer::LAST=previous_layer;

    // Actual test code:
    HDF5HistoryBasedPredictionEngine h5pe;
    std::vector<HDF5Event> events;
    int i;

    events.emplace_back(HDF5Event(node[3]->input, EventType::READ_DATASET));
    auto schema = h5pe.predict(events);

    for (auto event_pair : schema) {
        std::cout << "time: " << event_pair->first << std::endl;
        auto event_input = event_pair->second.input;
        for (i = 0; i < event_input.rank_; i++) {
            std::cout << "file_start_[" << i << "]: ";
            std::cout << event_input.file_start_[i] << std::endl;
            std::cout << "file_end_[" << i << "]: ";
            std::cout << event_input.file_end_[i] << std::endl;
        }
    }
}
