//
// Created by hariharan on 1/14/19.
//

#ifndef PROJECT_TIMER_H
#define PROJECT_TIMER_H


#include <chrono>
#include <string>

class Timer {
public:
    Timer():elapsed_time(0){}
    void startTime();
    double endTimeWithPrint(std::string fnName);
    double stopTime();
    double pauseTime();
    int resumeTime();
    double elapsed_time;
private:
    std::chrono::high_resolution_clock::time_point t1;

};



#endif //PROJECT_TIMER_H
