//
// Created by hariharan on 1/14/19.
//

#include "thread.h"

double
ThreadBenchmark::run_with_prefetch(really_long main_loop_count, really_long child_loop_count, uint prefetch_cores, bool prefetch, bool affinity) {

    unsigned num_cpus = std::thread::hardware_concurrency();
    --num_cpus;
    std::vector<std::thread> threads(num_cpus + prefetch_cores);
    for (unsigned i = 0; i < num_cpus; ++i) {
        if(i < prefetch_cores){
            threads[i] = std::thread(main_function,main_loop_count,elements_,data_,i+1);
            threads[num_cpus + i] = std::thread(prefetch_function,child_loop_count,elements_,data_,i);
        }
        else{
            threads[i] = std::thread(main_function,main_loop_count,elements_,data_,i);
        }
    }
    main_function(main_loop_count,elements_,data_,num_cpus + prefetch_cores + 1);
    for (auto& t : threads) {
        t.join();
    }
    return 0;
}


