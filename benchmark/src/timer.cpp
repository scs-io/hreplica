//
// Created by hariharan on 1/14/19.
//

#include "timer.h"
#include <iostream>
#include <iomanip>

void Timer::startTime() {
    t1 = std::chrono::high_resolution_clock::now();
}

double Timer::endTimeWithPrint(std::string fnName) {
    auto t2 = std::chrono::high_resolution_clock::now();
    auto t =  std::chrono::duration_cast<std::chrono::nanoseconds>(
            t2 - t1).count()/1000000000.0;
    if( t > 0.001){
        printf("%s : %lf\n",fnName.c_str(),t);
    }
    return t;
}

double Timer::stopTime() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now()-t1).count()/1000000000.0;
}

double Timer::pauseTime() {
    elapsed_time+=std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now()-t1).count()/1000000000.0;
    return elapsed_time;
}

int Timer::resumeTime() {
    t1 = std::chrono::high_resolution_clock::now();
    return 0;
}