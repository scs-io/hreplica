/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hermes.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose:Implements the Buffer API's for Hermes Platform
*
*-------------------------------------------------------------------------
*/
#include <hermes.h>
#include <core/buffer_api.h>

typedef BufferAPI<Matrix, HDF5Input,HDF5Output,HDF5WriteInput,HDF5WriteOutput,HDF5ReadInput,HDF5ReadOutput,HDF5Event,HDF5GraphNode> H5_Buffer;

herr_t H5_BufferInit(char* filename,
                     char* dataset_name,
                     int rank_,
                     int64_t type_,
                     hsize_t* dimentions_,
                     hsize_t* max_dimentions_,
                     void* dataset_object,
                     hid_t vol_id) {
    HDF5Input input;
    input.filename=bip::string(filename);
    input.dataset_name=bip::string(dataset_name);;
    input.rank_=rank_;
    input.type_=type_;
    input.layer=*Layer::LAST;
    input.dataset_object=dataset_object;
    input.vol_id=vol_id;
    for(int r=0;r<rank_;r++){
        input.dimentions_[r]=dimentions_[r];
        input.max_dimentions_[r]=max_dimentions_[r];
    }
    AutoTrace trace = AutoTrace("H5_BufferInit",input);
    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Init(InterfaceType::HDF5);
    herr_t output=bufferAPI->BufferInit(input).error;
    return output;
}

herr_t H5_BufferWrite(char* filename,
                      char* dataset_name,
                      int rank_,
                      int64_t type_,
                      hsize_t* file_start_,
                      hsize_t* file_end_,
                      hsize_t* buf_start_,
                      hsize_t* buf_end_,
                      void* dataset_object,
                      hid_t vol_id,
                      void * buffer) {
    HDF5WriteInput input;
    input.filename=std::string(filename);
    input.dataset_name=std::string(dataset_name);
    input.rank_=rank_;
    input.type_=type_;
    input.HDF5Input::buffer=buffer;
    input.HDF5Input::layer=*Layer::LAST;
    input.dataset_object=dataset_object;
    input.vol_id=vol_id;
    for(int r=0;r<rank_;r++){
        input.file_start_[r]=file_start_[r];
        input.file_end_[r]=file_end_[r];
        input.memory_start_[r]=buf_start_[r];
        input.memory_dim_[r]=buf_end_[r]-1;
    }
    AutoTrace trace = AutoTrace("H5_BufferWrite",input);

    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Init(InterfaceType::HDF5);
    herr_t output=bufferAPI->BufferWrite(input).error;
    return output;
}

herr_t H5_BufferRead(char* filename,
                             char* dataset_name,
                             int rank_,
                             int64_t type_,
                             hsize_t* file_start_,
                             hsize_t* file_end_,
                             hsize_t* buf_start_,
                             hsize_t* buf_dim_,
                             void* dataset_object,
                             hid_t vol_id,
                             void * buffer) {
    HDF5ReadInput input;
    input.filename=std::string(filename);
    input.dataset_name=std::string(dataset_name);
    input.rank_=rank_;
    input.type_=type_;
    input.HDF5Input::layer=*Layer::LAST;
    input.HDF5Input::buffer=buffer;
    input.dataset_object=dataset_object;
    input.vol_id=vol_id;
    for(int r=0;r<rank_;r++){
        input.file_start_[r]=(file_start_[r]);
        input.file_end_[r]=(file_end_[r]);
        input.memory_start_[r]=(buf_start_[r]);
        input.memory_dim_[r]=(buf_dim_[r]);
        input.dimentions_[r]=(buf_dim_[r]);
        input.max_dimentions_[r]=(buf_dim_[r]);
    }
    AutoTrace trace = AutoTrace("H5_BufferRead",input);
    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Init(InterfaceType::HDF5);
    herr_t output = bufferAPI->BufferRead(input).error;
    return output;
}

herr_t H5_BufferSync(char* filename,  char* dataset_name,int rank_,
                     hsize_t *dims,
                     hsize_t *max_dims,void* dataset_object,
                     hid_t vol_id) {
    HDF5Input input;
    input.filename=std::string(filename);
    input.dataset_name=std::string(dataset_name);
    input.dataset_object=dataset_object;
    input.vol_id=vol_id;
    input.rank_=rank_;
    for(int r=0;r<rank_;r++){
        input.file_start_[r]=(0);
        input.file_end_[r]=(dims[r]-1);
        input.memory_start_[r]=(0);
        input.memory_dim_[r]=(dims[r]-1);
        input.dimentions_[r]=(dims[r]);
        input.max_dimentions_[r]=(max_dims[r]);
    }
    AutoTrace trace = AutoTrace("H5_BufferSync",input);
    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Init(InterfaceType::HDF5);
    herr_t output = bufferAPI->BufferSync(input).error;
    return output;
}

herr_t H5_UpdateLayer(LayerInfo* layers,int count) {
    AutoTrace trace = AutoTrace("H5_UpdateLayer",count);
    Layer* current_layer=NULL;
    Layer* previous_layer=NULL;
    for(int order=0;order<count;order++){
        current_layer=new Layer();
        if(order==0){
            Layer::FIRST=current_layer;
        }
        current_layer->id_=order+1;
        current_layer->capacity_mb_=layers[order].capacity_mb_;
        current_layer->io_client_type=layers[order].is_memory?IOClientType::HDF5_MEMORY:IOClientType::HDF5_FILE;
        current_layer->direct_io=layers[order].direct_io;
        current_layer->bandwidth_mbps_=layers[order].bandwidth;
        strcpy(current_layer->layer_loc.data(),layers[order].mount_point_);
        current_layer->previous=previous_layer == NULL?nullptr:previous_layer;
        current_layer->next= nullptr;
        if(previous_layer != NULL){
            previous_layer->next=current_layer;
        }
        previous_layer=current_layer;
    }
    Layer::LAST=previous_layer;
    return 0;
}

herr_t H5_CleanBuffer() {
    AutoTrace trace = AutoTrace("H5_CleanBuffer",0);
    Layer* current_layer=Layer::FIRST;
    while(current_layer){
        Layer* next=current_layer->next;
        delete(current_layer);
        current_layer=next;
    }
    return 0;
}

int H5_Init() {
    AutoTrace trace = AutoTrace("H5_Init",0);
    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Init(InterfaceType::HDF5);
    HERMES_CONF->Configure();
    H5_UpdateLayer(HERMES_CONF->layers,HERMES_CONF->num_layers);
    return 0;
}

int H5_Finalize() {
    AutoTrace trace = AutoTrace("H5_Finalize",0);
    std::shared_ptr<H5_Buffer> bufferAPI=Singleton<H5_Buffer>::GetInstance();
    bufferAPI->Finalize();
    return 0;
}