//
// Created by hariharan on 5/5/20.
//

#include <core/hdf5_impl/replication/requirement_analyzer.h>
#ifdef ENABLE_COMPRESSION
#include <ares/filter/include/ares_filter/compression_metrics_manager.h>
#endif
#ifdef ENABLE_REPLICATION
ReplicationMetrics RequirementAnalyzer::analyze(HDF5WriteInput &input, ReplicationRequirement requirement, ReplicationConstraint constraint) {
    auto metrics = ReplicationMetrics();
    metrics.target_storage_used_ = input.GetSize();
    switch(requirement){
        case ReplicationRequirement::LOW_READ_LATENCY:{
            metrics.mode_ = ReplicationMode::SYNC;
            metrics.replica_count_ = REPLICA_DEFAULT;
            metrics.requirement_=requirement;
            metrics.constraint_ = constraint;
            for(int i=0;i<metrics.replica_count_;++i){
                if(i == 0) metrics.replication_hints_.push_back(ReplicationHint::NO_COMPRESSION);
                else metrics.replication_hints_.push_back(ReplicationHint::DEFAULT);
            }
#ifdef ENABLE_COMPRESSION
            metrics.weight_compression_ratio_ = 0.2;
            metrics.weight_decompression_time_ = 0.8;
            metrics.weight_compression_time_ = 0.0;
#endif
            break;
        }
        case ReplicationRequirement::HIGH_READ_BANDWIDTH:{
            metrics.mode_ = ReplicationMode::SYNC;
            metrics.replica_count_ = REPLICA_DEFAULT;
            metrics.requirement_ = requirement;
            metrics.constraint_ = constraint;
            for(int i=0;i<metrics.replica_count_;++i){
                metrics.replication_hints_.push_back(ReplicationHint::DEFAULT);
            }
#ifdef ENABLE_COMPRESSION
            metrics.weight_compression_ratio_ = 0.5;
            metrics.weight_decompression_time_ = 0.5;
            metrics.weight_compression_time_ = 0.0;
#endif
            break;
        }
        case ReplicationRequirement::HIGH_DURABILITY:{
            float max_r=0;

#ifdef ENABLE_COMPRESSION
            CompressionLibrary selected;
            for(int comp_lib_id = 0; comp_lib_id < NUM_ARES_LIBRARIES; comp_lib_id++) {
                auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
                //Get constants
                float tc, r, td;
                int type_id = cm_manager->GetDatasetType(input.type_);
                cm_manager->GetCompressionLibraryFit(type_id, comp_lib_id, tc, r, td);
                if(max_r < r){
                    max_r = r;
                    selected = static_cast<CompressionLibrary>(comp_lib_id);
                }
            }
            metrics.library_ = selected;
#endif

            metrics.mode_ = ReplicationMode::SYNC;
            metrics.replica_count_ = metrics.target_storage_used_/max_r;
            metrics.requirement_ = requirement;
            metrics.constraint_ = constraint;
            for(int i=0;i<metrics.replica_count_;++i){
                metrics.replication_hints_.push_back(ReplicationHint::SPECIFIC_COMPRESSION);
            }
#ifdef ENABLE_COMPRESSION
            metrics.weight_compression_ratio_ = 1.0;
            metrics.weight_decompression_time_ = 0.0;
            metrics.weight_compression_time_ = 0.0;
#endif
            break;
        }
        case ReplicationRequirement::LOAD_BALANCE:{
            metrics.mode_ = ReplicationMode::ASYNC;
            metrics.replica_count_ = REPLICA_DEFAULT;
            metrics.constraint_ = constraint;
            metrics.requirement_ = requirement;
            for(int i=0;i<metrics.replica_count_;++i)
                metrics.replication_hints_.push_back(ReplicationHint::DEFAULT);
#ifdef ENABLE_COMPRESSION
            metrics.weight_compression_ratio_ = 0.4;
            metrics.weight_decompression_time_ = 0.3;
            metrics.weight_compression_time_ = 0.3;
#endif
            break;
        }
        case ReplicationRequirement::LOW_ENERGY:{
            metrics.mode_ = ReplicationMode::ASYNC;
            metrics.replica_count_ = REPLICA_DEFAULT;
            metrics.requirement_ = requirement;
            metrics.constraint_ = constraint;
            for(int i=0;i<metrics.replica_count_;++i){
                metrics.replication_hints_.push_back(ReplicationHint::DEFAULT);
            }
#ifdef ENABLE_COMPRESSION
            metrics.weight_compression_ratio_ = 1.0;
            metrics.weight_decompression_time_ = 0.0;
            metrics.weight_compression_time_ = 0.0;
#endif
            break;
        }
        default:{
            break;
        }
    }
    switch(constraint){
        case ReplicationConstraint::LOW_STORAGE:{
            float max_r=0;
#ifdef ENABLE_COMPRESSION
            CompressionLibrary selected;
            for(int comp_lib_id = 0; comp_lib_id < NUM_ARES_LIBRARIES; comp_lib_id++) {
                auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
                //Get constants
                float tc, r, td;
                int type_id = cm_manager->GetDatasetType(input.type_);
                cm_manager->GetCompressionLibraryFit(type_id, comp_lib_id, tc, r, td);
                if(max_r < r){
                    max_r = r;
                    selected = static_cast<CompressionLibrary>(comp_lib_id);
                }
            }
            metrics.library_ = selected;
#endif
            for(int i=0;i<metrics.replica_count_;++i)
                if(metrics.replication_hints_[i] == ReplicationHint::DEFAULT)
                    metrics.replication_hints_[i] = ReplicationHint::SPECIFIC_COMPRESSION;
            break;
        }
        case ReplicationConstraint::LOW_WRITE_LATENCY:{
#ifdef ENABLE_COMPRESSION
            if(metrics.weight_decompression_time_ > 0){
                metrics.weight_decompression_time_=0.4;
                metrics.weight_compression_time_=0.4;
                metrics.weight_compression_ratio_=0.2;
            }else{
                metrics.weight_decompression_time_=0;
                metrics.weight_compression_time_=0.8;
                metrics.weight_compression_ratio_=0.2;
            }
#endif
            break;
        }
        case ReplicationConstraint::HIGH_WRITE_BANDWIDTH:{
#ifdef ENABLE_COMPRESSION
            if(metrics.weight_decompression_time_ > 0){
                metrics.weight_decompression_time_=0.4;
                metrics.weight_compression_time_=0.4;
                metrics.weight_compression_ratio_=0.2;
            }else{
                metrics.weight_decompression_time_=0;
                metrics.weight_compression_time_=0.5;
                metrics.weight_compression_ratio_=0.5;
            }
#endif
            break;
        }
        default:{
            break;
        }
    }
    input.replication_metrics = metrics;
    return metrics;
}
#endif