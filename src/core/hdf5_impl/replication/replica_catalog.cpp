//
// Created by hariharan on 5/5/20.
//

#include "core/hdf5_impl/replication/replica_catalog.h"
#ifdef ENABLE_REPLICATION
int ReplicaCatalog::Record(HDF5Input &source_original, std::vector<Placement<Matrix,HDF5Input,HDF5Output>>& replica_inputs) {
    auto replica_locations = std::vector<HDF5Input>();
    for(auto replica_input_placement:replica_inputs){
        auto replica_source_input = replica_input_placement.user_info;
        replica_source_input.is_replica=true;
        replica_source_input.replica_index=replica_input_placement.destination_info.replica_index;
        replica_locations.push_back(replica_input_placement.destination_info);
        metadataManager->UpdateOnWrite(replica_source_input, replica_input_placement.destination_info);
    }
    auto iter = replica_meta.Get(source_original.GetPath());
    if(iter.second < replica_locations.size())
        replica_meta.Put(source_original.GetPath(),replica_locations.size());
    return 0;
}

HDF5Input ReplicaCatalog::FindClosest(HDF5Input input) {
    Location my_location = GetMyLocation();
    auto iter = replica_meta.Get(input.GetPath());
    if(iter.first){
        auto replica_count = iter.second;
        /**
         * original copy
         */
        HDF5Input selected=input;
        size_t min_distance = CalculateDistance(my_location, selected);
        /**
        * replica copy
        */
        auto replica_input = input;
        replica_input.is_replica=true;
        for(int replica_index=0;replica_index<replica_count;++replica_index){
            replica_input.replica_index=replica_index;
            size_t distance = CalculateDistance(my_location, replica_input);
            if(distance < min_distance){
                min_distance = distance;
                selected = replica_input;
            }
        }
        return selected;
    }
    return HDF5Input();
}

Location ReplicaCatalog::GetMyLocation() {
    auto location = Location();
    location.node_id_ = HERMES_CONF->rank/HERMES_CONF->RANKS_PER_SERVER;
    return location;
}

Location ReplicaCatalog::BuildLocation(HDF5Input input) {
    auto location = Location();
    auto hash = std::hash<CharStruct>();
    location.node_id_ = hash(input.GetPath()) % HERMES_CONF->NUM_SERVERS;
    location.layer_index_ = input.layer.id_;
    return location;
}

double ReplicaCatalog::CalculateDistance(Location my_location, HDF5Input source_input) {
    auto buffered_datas = Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->FindBufferedData(source_input);
    size_t sum = 0;
    for(auto buffer_data:buffered_datas){
        sum += buffer_data.second.score;
    }
    return sum/(buffered_datas.size()*1.0);
}
#endif