//
// Created by HariharanDevarajan on 2/7/2019.
//

#include <src/core/interfaces/prefetcher/trigger_manager.h>
#include <core/hdf5_impl/prefetcher/hdf5_event_builder.h>
vector<HDF5Event> HDF5EventBuilder::build(HDF5Input input, EventType type,uint64_t sequenceId=0, bool is_end=false){
    vector<HDF5Event> events=vector<HDF5Event>();
    HDF5Event event;
    event.processId=rank;
    event.sequenceId= sequenceId == 0 ? globalSequence.GetNextSequenceServer(0):sequenceId;
//    event.time_stamp= Singleton<basket::global_clock>::GetInstance()->GetTime();
    event.input=input;
    event.type=type;
    event.is_end=is_end;
    events.push_back(event);
    return events;
}
