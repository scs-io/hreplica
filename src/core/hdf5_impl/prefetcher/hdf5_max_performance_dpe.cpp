//
// Created by HariharanDevarajan on 2/15/2019.
//

#include <core/hdf5_impl/prefetcher/hdf5_max_performance_dpe.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_max_performance_dpe.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definitions of data_placement_engine on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <cmath>
#include <core/metadata_manager_factory.h>
#include <core/hdf5_impl/hdf5_max_performance_dpe.h>
#include <common/constants.h>

std::vector<Placement<Matrix,HDF5Input,HDF5Output>>
PrefetchHDF5MaxPerformanceDataPlacementEngine::place(HDF5Input input) {

    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::PlaceWriteData",input);
    Matrix bufferDim;
    bufferDim.GetDefault(input.rank_);
    input.dimentions_=input.GetCount();
    input.max_dimentions_=input.GetCount();
    for(int i=0;i<input.rank_;i++){
        bufferDim.start_[i]=input.memory_start_[i];
        bufferDim.end_[i]=input.memory_dim_[i];
    }
    /* Place data optimally into first layer */
    std::vector<Placement<Matrix,HDF5Input,HDF5Output>> placements=CalculatePlacementOptimally(input,*Layer::FIRST,bufferDim);
    return placements;
}

std::vector<Placement<Matrix,HDF5Input, HDF5Output>>
PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally(HDF5Input input,Layer layer,
                                                                   const Matrix &buffer_dims) {
    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally",input,layer,buffer_dims);
    /* if layer has capacity to fit data or is the last layer. */
    size_t used_capacity=0;
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,layer,used_capacity) || layer.next == nullptr){
        /* Place data and calculate score. */
        auto placements=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        auto placement=BuildPlacement(input,layer,layer.next != nullptr,true,buffer_dims);
        placement.score_=CalculatePlacementScore(input,layer);
        placements.push_back(placement);
        return placements;
    }else if(HDF5DataOrganizer::GetInstance()->CanFit(input,layer)){
        /* if layer doesnt have remaining capacity but can fit data if data is moved. */
        auto original_buffer=buffer_dims;
        /*Score for making space + placement*/
        float move_and_place_score=CalculateMovingScore(input,layer,*layer.next)+CalculatePlacementScore(input,layer);
        /* cost of placing complete data on next layer */
        auto placments_sub = CalculatePlacementOptimally(input,*layer.next,buffer_dims);
        float next_layer_score=0;
        auto placements=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        for(auto placement:placments_sub){
            placements.push_back(placement);
            next_layer_score+=placement.score_;
        }
        /* take minimum of two and perform that placement*/
        if(next_layer_score < move_and_place_score) return placements;
        placements.clear();
        auto placement=BuildPlacement(input,layer,true,true,original_buffer);
        placement.score_=move_and_place_score;
        placements.push_back(placement);
        return placements;
    }else{
        /* if complete data cannot be fit in layer */
        /* split input into smaller piece that can fit in layer and then fit all other pieces in other layer. */
        auto placements_1=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        auto placements_2=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        std::vector<HDF5Input> inputs=SplitInput(input,layer.capacity_mb_);
//        int64_t remaining_capacity = 0;
//        if(layer.capacity_mb_*MB > HDF5DataOrganizer::GetInstance()->GetCurrentCapacity(layer))
//            remaining_capacity=layer.capacity_mb_*MB - HDF5DataOrganizer::GetInstance()->GetCurrentCapacity(layer);
        float move_and_place_score=CalculateMovingScore(inputs[0],layer,*layer.next)+CalculatePlacementScore(inputs[0],layer);
//        if(inputs[0].GetSize() >= remaining_capacity) remaining_capacity = 0;
//        else remaining_capacity-=inputs[0].GetSize();
        auto placement=BuildPlacement(inputs[0],layer,true,true,buffer_dims);
        placement.score_=move_and_place_score;
        placements_1.push_back(placement);
        auto placments_sub=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        for(int i=1;i<inputs.size();i++){
//            if(remaining_capacity > 0){
//                auto temp_placement=CalculatePlacementOptimally(inputs[i],layer,buffer_dims);
//                if(inputs[i].GetSize() >= remaining_capacity) remaining_capacity = 0;
//                else remaining_capacity -= inputs[0].GetSize();
//                placments_sub.insert(placments_sub.end(),temp_placement.begin(),temp_placement.end());
//            }else{
            auto temp_placement=CalculatePlacementOptimally(inputs[i],*layer.next,buffer_dims);
            placments_sub.insert(placments_sub.end(),temp_placement.begin(),temp_placement.end());
            //}
        }
        for(auto placement:placments_sub){
            placements_1.push_back(placement);
            move_and_place_score+=placement.score_;
        }
        float next_layer_score=0;
        placments_sub=CalculatePlacementOptimally(input,*layer.next,buffer_dims);
        for(auto placement:placments_sub){
            placements_2.push_back(placement);
            next_layer_score+=placement.score_;
        }
        /* if moving is more costly than placing in that layer then place data in next layer. */
        if(move_and_place_score < next_layer_score) return placements_1;
        else return placements_2;
    }
}

float
PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore(HDF5Input input, Layer layer) {
    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore",input,layer);
    /* Score is a function of time taken to execute I/O on layer*/
    uint64_t size_of_input=input.GetSize();
    float size_of_input_mb=size_of_input/MB;
    float time = size_of_input_mb/layer.bandwidth_mbps_;
    return time;
}

float
PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore(HDF5Input input, Layer source_layer,
                                                                  Layer destination_layer) {
    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore",input,source_layer,destination_layer);
    size_t used_capacity = 0;
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,source_layer,used_capacity))
        return 0;
    uint64_t size_of_input=input.GetSize();
    float size_of_input_mb=size_of_input/MB;
    /*
     * time = time to read from source layer + time to write to destination layer.
     */
    float time=size_of_input_mb/source_layer.bandwidth_mbps_ + size_of_input_mb/destination_layer.bandwidth_mbps_;
    return time;
}

Placement<Matrix,HDF5Input, HDF5Output>
PrefetchHDF5MaxPerformanceDataPlacementEngine::BuildPlacement(HDF5Input input,
                                                      Layer layer,
                                                      bool move_required,
                                                      bool generate_name,
                                                      const Matrix &buffer_dims) {
    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::BuildPlacement",input,layer,move_required,generate_name,buffer_dims);
    Placement<Matrix,HDF5Input, HDF5Output> placement;
    placement.move_required_=move_required;
    placement.output=HDF5Output();
    placement.destination_info=input;
    placement.user_info=input;
    placement.source_info=input;
    if(layer==*Layer::LAST){
        for(int rank=0;rank<input.rank_;rank++){
            placement.source_info.file_start_[rank] = input.file_start_[rank]-buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
        }
    }else{
        placement.source_info = placement.destination_info;
        if(generate_name)
            placement.destination_info.filename=Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->GenerateBufferFilename(input);
        for(int rank=0;rank<input.rank_;rank++){
            placement.destination_info.file_end_[rank] = placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank];
            placement.destination_info.file_start_[rank]=0;
            placement.destination_info.dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.destination_info.max_dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.source_info.file_start_[rank] = input.file_start_[rank]-buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
            placement.source_info.dimentions_[rank]=buffer_dims.end_[rank] > placement.source_info.file_end_[rank]?buffer_dims.end_[rank]:placement.source_info.file_end_[rank]+1;
            placement.source_info.max_dimentions_[rank]=placement.source_info.dimentions_[rank]+1;
        }
        placement.destination_info.layer=layer;
    }

    return placement;
}

std::vector<HDF5Input>
PrefetchHDF5MaxPerformanceDataPlacementEngine::SplitInput(HDF5Input input, uint64_t capacity_) {
    AutoTrace trace = AutoTrace("PrefetchHDF5MaxPerformanceDataPlacementEngine::SplitInput",input,capacity_);
    HDF5WriteInput input_1(input);
    uint64_t size_per_cell=0;
    uint64_t total_elements=1;
    for(int i=0;i<input.rank_;i++){
        size_per_cell+=sizeof(input.type_);
        total_elements*=input.file_end_[i]-input.file_start_[i];
    }
    uint64_t total_size=input.GetSize();
    /* calculate number of elements per dim allowed. */
    uint32_t num_elements_to_keep_per_dim = static_cast<uint32_t>(floor(pow(capacity_ * MB / size_per_cell,1./input.rank_)));

    std::vector<HDF5Input> inputs=std::vector<HDF5Input>();

    std::vector<HDF5WriteInput> pieces=std::vector<HDF5WriteInput>();
    Matrix left_matrix=Matrix(input.rank_,input.file_start_,input.file_end_);
    std::vector<HDF5Input> others=std::vector<HDF5Input>();
    /* split orginal input into smaller chunks. */
    for(int i=0;i<input.rank_;i++){
        if(num_elements_to_keep_per_dim < input_1.file_end_[i]-input_1.file_start_[i]+1){
            input_1.file_end_[i] = input_1.file_start_[i] + num_elements_to_keep_per_dim - 1;
            if(input_1.file_end_[i]<input.file_end_[i]){
                HDF5WriteInput input_2(input);
                input_2.file_start_[i]=input_1.file_end_[i]+1;
                left_matrix.end_[i]=input_2.file_start_[i]-1;
                others.push_back(input_2);
            }
        }
    }
    inputs.push_back(input_1);
    inputs.insert(inputs.end(),others.begin(),others.end());
    return inputs;
}

