//
// Created by hariharan on 6/7/18.
//

#include <debug.h>
#include <common/constants.h>
#include <core/metadata_manager_factory.h>
#include <core/hdf5_impl/hdf5_memory.h>


HDF5Output HDF5Memory::Create(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5Memory::Create",input);
    return HDF5Output();
}

HDF5Output HDF5Memory::Read(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5Memory::Read",source,destination);
    HDF5ReadOutput read_output;
    DBGVAR(source.filename);
    std::string buffered_file=source.HDF5Input::layer.layer_loc+FILE_PATH_SEPARATOR+source.filename;
    auto iterator = existing_file.Get(buffered_file);
    if(iterator.first){
        FileInfo info = iterator.second;
        destination.HDF5Input::buffer = info.data.data();
    }else read_output.error=-1;

    return read_output;
}


HDF5Output HDF5Memory::Write(HDF5Input source, HDF5Input destination) {
    AutoTrace trace = AutoTrace("HDF5Memory::Write",source,destination);
    DBGVAR2(destination.filename,destination.HDF5Input::layer.layer_loc);
    HDF5WriteOutput write_output;
    std::string buffered_file=std::string(destination.HDF5Input::layer.layer_loc)+FILE_PATH_SEPARATOR+destination.filename;
    /*write buffered data*/
    auto iterator = existing_file.Get(buffered_file);
    if(!iterator.first){
        FileInfo info;
        info.size=destination.GetSize();
        info.matrix=Matrix(destination.rank_, destination.file_start_, destination.file_end_);
        info.data=std::string((char*)source.HDF5Input::buffer);
        existing_file.Put(buffered_file,info);
    }else{
        FileInfo info = iterator.second;
        if(info.matrix == Matrix(destination.rank_, destination.file_start_, destination.file_end_)){
            info.data=std::string((char*)source.HDF5Input::buffer);
        }
    }
    return write_output;
}

HDF5Output HDF5Memory::Delete(HDF5Input input) {
    AutoTrace trace = AutoTrace("HDF5Memory::Delete",input);
    std::string buffered_file=std::string(input.layer.layer_loc)+FILE_PATH_SEPARATOR+input.filename;
    HDF5Output output;
    if(FileExists(buffered_file.c_str())){
        auto existing_file_iterator=existing_file.Erase(buffered_file);
    }
    return output;
}

int64_t HDF5Memory::GetCurrentCapacity(Layer layer) {
    AutoTrace trace = AutoTrace("HDF5Memory::GetCurrentCapacity",layer);
    int64_t used_capacity=0;
    auto matched_files=existing_file.GetAllData();
    for(std::pair<std::string,FileInfo> matched_file:matched_files){
        used_capacity+=matched_file.second.size;
    }
    return used_capacity;
}
