/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: hdf5_max_performance_dpe.cpp
* June 5 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: It is the definitions of data_placement_engine on hdf5 file format.
*
*-------------------------------------------------------------------------
*/

#include <cmath>
#include <core/metadata_manager_factory.h>
#ifdef ENABLE_COMPRESSION
#include <ares_filter/configuration_manager.h>
#include <ares_filter/compression_metrics_manager.h>
#endif
#include <core/hdf5_impl/hdf5_max_performance_dpe.h>
#include <common/constants.h>
#include <common/configuration_manager.h>


std::vector<Placement<Matrix,HDF5Input,HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::PlaceReadData(HDF5ReadInput input) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::PlaceReadData",input);
    /* Get buffered data from MDM*/
    std::vector<std::pair<HDF5Input,HDF5Input>> buffered_data = Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->FindBufferedData(input);
    /* Construct and return placements */
    auto placements = std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
    if(buffered_data.size()!=0){
        for(std::pair<HDF5Input,HDF5Input> pair:buffered_data){
            Placement<Matrix,HDF5Input, HDF5Output> placement;
            placement.move_required_=false;
            placement.output=HDF5Output();
            placement.source_info=pair.first;
            placement.destination_info=pair.second;
            placements.push_back(placement);
        }
    }
    return placements;
}

std::vector<Placement<Matrix,HDF5Input,HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData(HDF5WriteInput input) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::PlaceWriteData",input);
    Matrix bufferDim;
    bufferDim.GetDefault(input.rank_);
    input.dimentions_=input.GetCount();
    input.max_dimentions_=input.GetCount();
    for(int i=0;i<input.rank_;i++){
        bufferDim.start_[i]=input.memory_start_[i];
        bufferDim.end_[i]=input.memory_dim_[i];
    }
    auto used_capacity = std::vector<size_t>();
    for(auto layer = Layer::FIRST; layer!= nullptr; layer=layer->next){
        used_capacity.push_back(0);
    }
    /* Place data optimally into first layer */
    CharStruct destination_filename =  Singleton<MetadataManagerFactory<Matrix,HDF5Input>>::GetInstance()->GetMDM(MDM_TYPE)->GenerateBufferFilename(input);
    auto placements=CalculatePlacementOptimally(input,*Layer::FIRST,bufferDim,used_capacity, destination_filename, false);
    return placements;
}


std::vector<Placement<Matrix,HDF5Input, HDF5Output>>
HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally(HDF5WriteInput input,Layer layer,
                                                                   Matrix &buffer_dims, std::vector<size_t> &used_capacity,
                                                                   CharStruct &destination_filename,
                                                                   bool is_replica)
{
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementOptimally",input,layer,buffer_dims);

    //if layer has capacity to fit data or is the last layer.
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,layer,used_capacity[layer.id_-1]) || layer.next == nullptr){
        auto optimal_compression = FindOptimalCompression(input,layer);
        auto original_buffer_dims = buffer_dims;
        float place_score=optimal_compression.first;
        auto placements = std::vector<Placement<Matrix,HDF5Input, HDF5Output>>();
        auto placement = BuildPlacement(input,layer,optimal_compression.first,false,optimal_compression.second,destination_filename,buffer_dims);
        placement.score_ = place_score;
        placements.push_back(placement);
        used_capacity[layer.id_-1] += placement.destination_info.GetSize();
#ifdef ENABLE_REPLICATION
        if(!input.is_replica){
            auto replica_placements = CalculateReplicaPlacement(input,layer,original_buffer_dims,used_capacity,destination_filename);
            for(auto placement:replica_placements){
                placements.push_back(placement);
            }
        }
#endif
        return placements;
    }
    //if layer doesnt have enough capacity but can fit data if data is moved.
    else if(HDF5DataOrganizer::GetInstance()->CanFit(input,layer)){
        //Score for making space + placing
        auto split_buffer_dims=buffer_dims;
        auto split_used_capacity = used_capacity;
        auto optimal_compression = FindOptimalCompression(input,layer);
        float movement_cost = CalculateMovingScore(input,layer,*layer.next,split_used_capacity[layer.id_-1]);
        float move_and_place_score = movement_cost + optimal_compression.first;
        auto split_placements = std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();

        auto placement = BuildPlacement(input,layer, move_and_place_score, movement_cost!=0,optimal_compression.second,destination_filename,split_buffer_dims);
        placement.score_=move_and_place_score;
        split_placements.push_back(placement);
        split_used_capacity[layer.id_-1] = placement.destination_info.GetSize();
#ifdef ENABLE_REPLICATION
        if(!input.is_replica) {
            auto replica_placements = CalculateReplicaPlacement(input, layer, split_buffer_dims, split_used_capacity,destination_filename);
            for (auto placement:replica_placements) {
                split_placements.push_back(placement);
                if (input.replication_metrics.mode_ == ReplicationMode::SYNC)
                    move_and_place_score += placement.score_;
            }
        }
#endif
        //Score for skipping this layer
        auto skip_buffer_dims=buffer_dims;
        auto skip_used_capacity = used_capacity;
        auto placments_sub = CalculatePlacementOptimally(input,*layer.next,skip_buffer_dims,skip_used_capacity,destination_filename,false);
        float next_layer_score=0;
        auto skip_placements = std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        for(auto placement:placments_sub){
            skip_placements.push_back(placement);
            next_layer_score+=placement.score_;
        }
#ifdef ENABLE_REPLICATION
        if(!input.is_replica) {
            auto replica_placements = CalculateReplicaPlacement(input, layer, split_buffer_dims, skip_used_capacity,destination_filename);
            for (auto placement:replica_placements) {
                split_placements.push_back(placement);
                if (input.replication_metrics.mode_ == ReplicationMode::SYNC)
                    move_and_place_score += placement.score_;
            }
        }
#endif
        if(
#ifdef ENABLE_REPLICATION
                input.replication_metrics.requirement_ != ReplicationRequirement::LOW_READ_LATENCY &&
#endif
            next_layer_score < move_and_place_score){
            used_capacity = skip_used_capacity;
            buffer_dims = skip_buffer_dims;
            return skip_placements;
        }else{
            used_capacity = split_used_capacity;
            buffer_dims = split_buffer_dims;
            return split_placements;
        }
    }
    //if complete data cannot be fit in layer
    else{
        //split input into smaller piece that can fit in layer and then fit all other pieces in other layer.
        auto placements_1=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        auto placements_2=std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        std::vector<HDF5Input> inputs=SplitInput(input,layer.capacity_mb_);

        //Score for splitting data and sending rest to next layer
        auto split_used_capacity = used_capacity;
        auto split_buffer_dims = buffer_dims;
        auto optimal_compression  = FindOptimalCompression(inputs[0],layer);
        float movement_cost = CalculateMovingScore(inputs[0],layer,*layer.next,split_used_capacity[layer.id_-1]);
        float move_and_place_score = movement_cost + optimal_compression.first;
        auto placement = BuildPlacement(inputs[0],layer,move_and_place_score,movement_cost!=0,optimal_compression.second,destination_filename,split_buffer_dims);
        placement.score_=move_and_place_score;
        placements_1.push_back(placement);
        split_used_capacity[layer.id_-1] += placement.destination_info.GetSize();
        auto placments_sub = std::vector<Placement<Matrix,HDF5Input,HDF5Output>>();
        for(int i=1;i<inputs.size();i++){
            auto temp_placement = CalculatePlacementOptimally(inputs[i],*layer.next,buffer_dims,split_used_capacity,destination_filename,false);
            placments_sub.insert(placments_sub.end(),temp_placement.begin(),temp_placement.end());
        }
        for(auto placement:placments_sub){
            placements_1.push_back(placement);
            move_and_place_score+=placement.score_;
        }
#ifdef ENABLE_REPLICATION
        if(!input.is_replica) {
            auto replica_placements = CalculateReplicaPlacement(input, layer, split_buffer_dims, split_used_capacity,destination_filename);
            for (auto placement:replica_placements) {
                placements_1.push_back(placement);
                if (input.replication_metrics.mode_ == ReplicationMode::SYNC)
                    move_and_place_score += placement.score_;
            }
        }
#endif
        //Score for skipping this layer
        auto skip_used_capacity = used_capacity;
        auto skip_buffer_dims = buffer_dims;
        float next_layer_score=0;
        placments_sub=CalculatePlacementOptimally(input,*layer.next,skip_buffer_dims,skip_used_capacity,destination_filename,false);
        for(auto placement:placments_sub){
            placements_2.push_back(placement);
            next_layer_score+=placement.score_;
        }
#ifdef ENABLE_REPLICATION
        if(!input.is_replica) {
            auto replica_placements = CalculateReplicaPlacement(input, layer, skip_buffer_dims, skip_used_capacity,destination_filename);
            for (auto placement:replica_placements) {
                placements_1.push_back(placement);
                if (input.replication_metrics.mode_ == ReplicationMode::SYNC)
                    next_layer_score += placement.score_;
            }
        }
#endif
        //if moving is more costly than placing in that layer then place data in next layer.
        if(
#ifdef ENABLE_REPLICATION
            input.replication_metrics.requirement_ == ReplicationRequirement::LOW_READ_LATENCY ||
#endif
            move_and_place_score < next_layer_score){
            used_capacity = split_used_capacity;
            buffer_dims = split_buffer_dims;
            return placements_1;
        }else{
            used_capacity = skip_used_capacity;
            buffer_dims = skip_buffer_dims;
            return placements_2;
        }
    }
}


std::pair<float,CompressionLibrary>
HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression(HDF5WriteInput input, Layer layer){
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression",input,layer);
    CompressionLibrary comp_lib=CompressionLibrary::DUMMY;
#ifndef ENABLE_COMPRESSION
    return make_pair<float,CompressionLibrary>(CalculatePlacementScore(input,layer,CompressionLibrary::DUMMY),CompressionLibrary::DUMMY);
#else
    if(ARES_FILTER_CONF->compressionLibrary != CompressionLibrary::DYNAMIC){
        return make_pair(CalculatePlacementScore(input,layer,ARES_FILTER_CONF->compressionLibrary),ARES_FILTER_CONF->compressionLibrary);
    }else {
        CompressionLibrary best_lib = CompressionLibrary::DUMMY;
        float min_time = std::numeric_limits<float>::infinity();
#ifdef ENABLE_REPLICATION
        if (input.replication_metrics.replication_hints_[input.replica_index] == ReplicationHint::NO_COMPRESSION) {
            auto time = CalculatePlacementScore(input, layer, CompressionLibrary::DUMMY);
            min_time = time;
            best_lib = CompressionLibrary::DUMMY;
        } else if (input.replication_metrics.replication_hints_[input.replica_index] == ReplicationHint::SPECIFIC_COMPRESSION){
            auto time = CalculatePlacementScore(input,layer,input.replication_metrics.library_);
            min_time = time;
            best_lib = input.replication_metrics.library_;
        }else
#endif
            {
            for(int comp_lib_id = 0; comp_lib_id < NUM_ARES_LIBRARIES; comp_lib_id++) {
                //Selection
                auto time = CalculatePlacementScore(input,layer,static_cast<CompressionLibrary>(comp_lib_id));
                if(time < min_time){
                    min_time = time;
                    best_lib = static_cast<CompressionLibrary>(comp_lib_id);
                }
            }
        }
        //std::cout << "HDF5MaxPerformanceDataPlacementEngine::FindOptimalCompression-{BEST_LIB: " << static_cast<int>(best_lib) << "}" << std::endl;
        return make_pair(min_time,best_lib);
    }
#endif
}

float HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore(HDF5Input input, Layer layer, CompressionLibrary comp_lib)
{
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore",input,layer);
    //Get file and layer information

    float S = input.GetSize();
    float Bi = layer.bandwidth_mbps_;

#ifndef ENABLE_COMPRESSION
    return S/Bi;
#else
    if(CompressionLibrary::DUMMY == comp_lib) return S/Bi;
    auto cm_manager = Singleton<CompressionMetricsManager>::GetInstance();
    //Get constants
    float c1, c2, c3;
    int comp_lib_id = static_cast<int>(comp_lib);
    int type_id = cm_manager->GetDatasetType(input.type_);
    cm_manager->GetCompressionLibraryFit(type_id, comp_lib_id, c1, c2, c3);
    //Estimates
    float tc = c1*S;
    float r = c2;
    float td = c3*S;
    //Score
    float time = ARES_FILTER_CONF->metric_weight[0]*tc + S/Bi - ARES_FILTER_CONF->metric_weight[1]*(S/Bi)*((r-1)/r) + ARES_FILTER_CONF->metric_weight[2]*td;
    //std::cout << "HDF5MaxPerformanceDataPlacementEngine::CalculatePlacementScore-{" << tc << "}-{" << r << "}-{" << td << "}-{" << S << "}-{" << time << "}\n";
    return time;
#endif
}

float
HDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore(HDF5Input input, Layer source_layer,
                                                                     Layer destination_layer, size_t &used_capacity) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::CalculateMovingScore",input,source_layer,destination_layer);
    if(HDF5DataOrganizer::GetInstance()->HasCapacity(input,source_layer,used_capacity))
        return 0;
    uint64_t size_of_input=input.GetSize();
    float size_of_input_mb=size_of_input/MB;
    /*
     * time = time to read from source layer + time to write to destination layer.
     */
    float time=size_of_input_mb/source_layer.bandwidth_mbps_ + size_of_input_mb/destination_layer.bandwidth_mbps_;
    return time;
}

Placement<Matrix,HDF5Input, HDF5Output>
HDF5MaxPerformanceDataPlacementEngine::BuildPlacement(HDF5Input input,
                                                      Layer layer,
                                                      float score,
                                                      bool move_required,
                                                      CompressionLibrary comp_lib,
                                                      CharStruct &destination_filename,
                                                      Matrix &buffer_dims) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::BuildPlacement",input,layer,move_required,destination_filename,buffer_dims);
    Placement<Matrix,HDF5Input, HDF5Output> placement;
    placement.move_required_=move_required;
    placement.output=HDF5Output();
    input.score=score;
    placement.destination_info=input;
    placement.user_info=input;
    placement.source_info=input;
    if(layer==*Layer::LAST){
        for(int rank=0;rank<input.rank_;rank++){
            placement.source_info.file_start_[rank] = input.file_start_[rank]-buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
        }
    }else{
        placement.destination_info.filename = destination_filename;
        placement.source_info = placement.destination_info;
        for(int rank=0;rank<input.rank_;rank++){
            placement.destination_info.file_end_[rank] = placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank];
            placement.destination_info.file_start_[rank]=0;
            placement.destination_info.dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.destination_info.max_dimentions_[rank]=placement.destination_info.file_end_[rank]-placement.destination_info.file_start_[rank]+1;
            placement.source_info.file_start_[rank] = buffer_dims.start_[rank];
            placement.source_info.file_end_[rank] = placement.source_info.file_start_[rank] + placement.destination_info.GetCount()[rank]-1;
            buffer_dims.start_[rank] = placement.source_info.file_end_[rank]+1;
            placement.source_info.dimentions_[rank]=buffer_dims.end_[rank] > placement.source_info.file_end_[rank]?buffer_dims.end_[rank]:placement.source_info.file_end_[rank]+1;
            placement.source_info.max_dimentions_[rank]=placement.source_info.dimentions_[rank]+1;
        }
        placement.destination_info.layer=layer;
    }
#ifdef ENABLE_COMPRESSION
    placement.source_info.comp_lib = comp_lib;
#endif
    return placement;
}

std::vector<HDF5Input>
HDF5MaxPerformanceDataPlacementEngine::SplitInput(HDF5Input input, uint64_t capacity_) {
    AutoTrace trace = AutoTrace("HDF5MaxPerformanceDataPlacementEngine::SplitInput",input,capacity_);
    int piece_index=input.pieces_count-1;
    HDF5WriteInput input_1(input);
    input_1.piece_index=piece_index++;
    uint64_t total_elements=1;
    for(int i=0;i<input.rank_;i++){
        total_elements*=input.file_end_[i]-input.file_start_[i];
    }
    uint64_t total_size=input.GetSize();
    /* calculate number of elements per dim allowed. */
    uint32_t num_elements_to_keep_per_dim = static_cast<uint32_t>(floor(pow(capacity_ * MB / H5Tget_size(input.type_),1./input.rank_)));

    std::vector<HDF5Input> inputs=std::vector<HDF5Input>();

    std::vector<HDF5WriteInput> pieces=std::vector<HDF5WriteInput>();
    Matrix left_matrix=Matrix(input.rank_,input.file_start_,input.file_end_);
    std::vector<HDF5Input> others=std::vector<HDF5Input>();
    /* split orginal input into smaller chunks. */
    for(int i=0;i<input.rank_;i++){
        if(num_elements_to_keep_per_dim < input_1.file_end_[i]-input_1.file_start_[i]+1){
            input_1.file_end_[i] = input_1.file_start_[i] + num_elements_to_keep_per_dim - 1;
            if(input_1.file_end_[i]<input.file_end_[i]){
                HDF5WriteInput input_2(input);
                input_2.file_start_[i]=input_1.file_end_[i]+1;
                left_matrix.end_[i]=input_2.file_start_[i]-1;
                input_2.piece_index=piece_index++;
                others.push_back(input_2);
            }
        }
    }
    input_1.pieces_count=piece_index;
    inputs.push_back(input_1);
    for(auto other:others){
        other.pieces_count=piece_index;
        inputs.push_back(other);
    }
    return inputs;
}
#ifdef ENABLE_REPLICATION
std::vector<Placement<Matrix,HDF5Input, HDF5Output>> HDF5MaxPerformanceDataPlacementEngine::CalculateReplicaPlacement(HDF5WriteInput input, Layer layer, Matrix &buffer_dims,
        vector<size_t> &used_capacity, CharStruct &destination_filename) {
    auto replicas_placements = std::vector<Placement<Matrix,HDF5Input, HDF5Output>>();
    for(int i=0; i<input.replication_metrics.replica_count_; i++){
        auto original_buffer_dims = buffer_dims;
        input.is_replica=true;
        input.replica_index=i;
        auto replica_placements = CalculatePlacementOptimally(input,layer,original_buffer_dims,used_capacity, destination_filename, true);
        for(auto placement:replica_placements){
            placement.destination_info.is_replica=true;
            placement.destination_info.replica_index=i;
            replicas_placements.push_back(placement);
        }
    }
    return replicas_placements;
}
#endif