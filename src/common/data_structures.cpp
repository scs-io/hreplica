/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5. The full HDF5 copyright notice, including      *
 * terms governing use, modification, and redistribution, is contained in    *
 * the files COPYING and Copyright.html. COPYING can be found at the root    *
 * of the source code distribution tree; Copyright.html can be found at the  *
 * root level of an installed copy of the electronic HDF5 document set and   *
 * is linked from the top-level documents page. It can also be found at      *
 * http://hdfgroup.org/HDF5/doc/Copyright.html. If you do not have           *
 * access to either file, you may request a copy from help@hdfgroup.org.     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*-------------------------------------------------------------------------
*
* Created: data_structures.cpp
* May 28 2018
* Hariharan Devarajan <hdevarajan@hdfgroup.org>
*
* Purpose: Defines all data structures required in the Hermes Platform.
*
*-------------------------------------------------------------------------
*/
#include <common/data_structures.h>
/**
 * Output streams for my data structures
 */
/*
 * initializes the static attributes within Layer class.
 */
Layer* Layer::FIRST = nullptr; /* ram */
Layer* Layer::LAST = nullptr;  /* pfs */

std::ostream &operator<<(std::ostream &os, char const *m) {
    return os << std::string(m);
}
std::ostream &operator<<(std::ostream &os, uint8_t const &m) {
    return os << std::to_string(m);
}
std::ostream &operator<<(std::ostream &os, Layer const &m) {
    return os   << "id_:" << m.id_ << ","
                << "capacity_mb_:" << m.capacity_mb_ << ","
                << "layer_loc:" << std::string(m.layer_loc.c_str()) << ","
                << "bandwidth_mbps_:" << m.bandwidth_mbps_ << ","
                << "io_client_type:" << m.io_client_type<<"}";
}

std::ostream &operator<<(std::ostream &os, Input<Data> const &m) {
    return os   << "{operation_type:" << m.operation_type <<","
                << "layer:" << m.layer<<"}";
}

std::ostream &operator<<(std::ostream &os, ReadInput<Data> const &m) {
    return os   << m;
}

std::ostream &operator<<(std::ostream &os, WriteInput<Data> const &m) {
    return os   << m;
}
std::ostream &operator<<(std::ostream &os, std::vector<hsize_t> const &m) {
    for (auto i = m.begin(); i != m.end(); ++i)
        os <<"["<< *i << ',';
    os <<"]";
    return os;
}

std::ostream &operator<<(std::ostream &os, bip::vector<hsize_t> const &m) {
    for (auto i = m.begin(); i != m.end(); ++i)
        os <<"["<< *i << ',';
    os <<"]";
    return os;
}

std::ostream &operator<<(std::ostream &os, std::array<hsize_t,MAX_DIMS> const &m) {
    for (auto i = m.begin(); i != m.end(); ++i)
        os <<"["<< *i << ',';
    os <<"]";
    return os;
}

std::ostream &operator<<(std::ostream &os, HDF5Input const &m) {
    return os   <<"{operation_type:" << m.operation_type <<","
                << "layer:" << m.layer<<","
            << "filename:" << m.filename <<","
            << "dataset_name:" << m.dataset_name <<","
            << "rank_:" << m.rank_ <<","
            << "type_:" << m.type_ <<","
            << "dimentions_:" << m.dimentions_ <<","
            << "max_dimentions_:" << m.max_dimentions_ <<","
            << "start_:" << m.file_start_ <<","
            << "end_:" << m.file_end_ <<"}";
}
std::ostream &operator<<(std::ostream &os, HDF5ReadInput const &m) {
    return operator<<(os,static_cast <const HDF5Input &>( m ));
}
std::ostream &operator<<(std::ostream &os, HDF5WriteInput const &m) {
    return operator<<(os,static_cast <const HDF5Input &>( m ));
}

std::ostream &operator<<(std::ostream &os, Output const &m) {
    return os   << "";
}

std::ostream &operator<<(std::ostream &os, WriteOutput const &m) {
    return os   << "";
}
std::ostream &operator<<(std::ostream &os, ReadOutput const &m) {
    return os   << "";
}
std::ostream &operator<<(std::ostream &os, HDF5Output const &m) {
    return os   << "{error:"<<m.error<<"}";
}
std::ostream &operator<<(std::ostream &os, HDF5WriteOutput const &m) {
    return operator<<(os,static_cast <const HDF5Output &>( m ));
}

std::ostream &operator<<(std::ostream &os, HDF5ReadOutput const &m) {
    return operator<<(os,static_cast <const HDF5Output &>( m ));
}

template<typename D, typename I, typename O,
        typename std::enable_if<std::is_base_of<Data, D>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Input<D>, I>::value>::type * = nullptr,
        typename std::enable_if<std::is_base_of<Output, O>::value>::type * = nullptr>
std::ostream &operator<<(std::ostream &os, Placement<D, I,O> const &m) {
    return os   << "{source_info:"<<m.source_info<<","
            << "destination_info:"<<m.destination_info<<","
            << "user_info:"<<m.user_info<<","
            << "output:"<<m.output<<","
            << "score_:"<<m.score_<<","
            << "move_required_:"<<m.move_required_<<"}";
}
std::ostream &operator<<(std::ostream &os, Data const &m) {
    return os   << "";
}

std::ostream &operator<<(std::ostream &os, Matrix const &m) {
    return os   << "{num_dimension:"<<m.num_dimension<<","
                       << "start_:"<<m.start_<<","
                       << "original_start_:"<<m.original_start_<<","
                       << "end_:"<<m.end_<<"}";
}