
#ifdef ENABLE_COMPRESSION
#include <ares_filter/configuration_manager.h>
#include <ares_filter/compression_metrics_manager.h>
#endif
#include <common/configuration_manager.h>

extern "C" {

void SetRanksPerServer(int RANKS_PER_SERVER){
    HERMES_CONF->RANKS_PER_SERVER = RANKS_PER_SERVER;
    HERMES_CONF->calculateServerValues();
}

void SetHermesConfiguration(char *config){
    HERMES_CONF->configuration_file = config;
}

void SetMetricsFile(char *metrics) {
#ifdef ENABLE_COMPRESSION
    ARES_FILTER_CONF->metrics_file = metrics;
#endif
}

void SetCompressionLibrary(int comp_lib) {
#ifdef ENABLE_COMPRESSION
    ARES_FILTER_CONF->compressionLibrary = static_cast<CompressionLibrary>(comp_lib);
#endif
}

void PrintCompressionMetrics(void) {
#ifdef ENABLE_COMPRESSION
    Singleton<CompressionMetricsManager>::GetInstance()->PrintCompressionMetrics();
#endif
}

}
