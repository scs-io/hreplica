
#ifndef _CONFIGURATION_GENERATOR_H_
#define _CONFIGURATION_GENERATOR_H_

#include <iostream>
#include <sstream>
#include <list>
#include <iterator>
#include <string>
#include <rapidjson/filereadstream.h>
#include <rapidjson/document.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/writer.h>
#include <hermes.h>

#define METADATA 1.2

struct TemplateArg {
    std::string input;
    std::string output;
    int net_size;
};

class ConfigurationGenerator
{
	int argc = 0;
	char **argv = nullptr;
	
	public:

    std::string config_path;
    std::string config_dir;
    std::string out_dir;
    std::list<TemplateArg> templates;

	/*CUNSTROCTORS*/

    ConfigurationGenerator(int argc, char **argv)
	{
        this->config_dir = "./";
        this->out_dir = "./";
        this->config_path = config_dir + "ConfigurationGenerator.json";
		this->argc = argc;
		this->argv = argv;

		Parse();
		ReadConfigurationGenerator();
	}
	
	
	private:
	
	/*PARSING COMMAND LINE ARGUMENTS*/
	
	void Parse(void)
	{
		for(int i = 1; i < argc; i++)
		{
			//----Help flag
			if(strcmp(argv[i], "-h") == 0)
			{
                HelpArg();
				continue;
			}

			//----Get configuration input directory
			else if(strcmp(argv[i], "-i") == 0)
			{
                ConfigInputDirectoryArg(i);
				continue;
			}

			//----Get configuration output directory
            else if(strcmp(argv[i], "-o") == 0)
            {
                ConfigOutputDirectoryArg(i);
                continue;
            }

			//----Invalid
			else
			{
                InvalidArg(i);
				continue;
			}
		}
	}
	
	
	/*INTERPRETING COMMAND LINE ARGUMENTS*/
	
	//-h
	void HelpArg(void)
	{
		std::cout << "-h: Print out help for this program.\n";
		std::cout << "-i: \"/path/to/template_directory/\": The directory containing ConfigurationGenerator.json.\n";
		std::cout << "-o: \"/path/to/output_directory/\": The directory that will contain the outputs of this program.\n";
	}

	//-i [directory]
	void ConfigInputDirectoryArg(int &i)
    {
	    ++i;
	    config_dir = argv[i];
        config_path = config_dir + "ConfigurationGenerator.json";
    }

    //-o [directory]
    void ConfigOutputDirectoryArg(int &i)
    {
        ++i;
        out_dir = argv[i];
    }

	//invalid argument
	void InvalidArg(int i)
	{
		std::cout << "Invalid argument: " << argv[i] << "\n";
	}


	/*READING CONFIGURATION FOR GENERATING HERMES CONFIGURATIONS*/

	void ReadConfigurationGenerator() {

	    //Open config file
	    FILE *config_file = fopen(config_path.c_str(), "r");
	    if(config_file == NULL) {
	        std::cout << "Configuration generator does not exist: " << config_path << std::endl;
	        return;
	    }

        //Initialize read stream
        char buf[65536];
        rapidjson::FileReadStream instream(config_file, buf, sizeof(buf));
        rapidjson::Document d;

        //Get JSON object
        d.ParseStream(instream);
        if(!d.IsArray()) {
            std::cout << "Configuration generator is invalid" << std::endl;
            fclose(config_file);
            return;
        }

        //Read in information
        for (rapidjson::Value::ConstValueIterator itr1 = d.Begin(); itr1 != d.End(); ++itr1) {
            //Get the JSON object containing config information
            const rapidjson::Value &conf = (rapidjson::Value & ) * itr1;
            //Make Sure io sizes and nreqs are JSON arrays
            assert(conf["io_sizes"].IsArray());
            assert(conf["nreqs"].IsArray());
            //Get the path to the template file
            std::string template_fn = conf["template"].GetString();
            std::string template_path = config_dir + template_fn + ".json";
            //Create a config for every possible size
            for (rapidjson::Value::ConstValueIterator itr2 = conf["io_sizes"].Begin(); itr2 != conf["io_sizes"].End(); ++itr2) {
                int io_size = ((rapidjson::Value & )*itr2).GetInt();
                for (rapidjson::Value::ConstValueIterator itr3 = conf["nreqs"].Begin(); itr3 != conf["nreqs"].End(); ++itr3) {
                    int nreqs = ((rapidjson::Value & )*itr3).GetInt();
                    TemplateArg arg;
                    arg.input = template_path;
                    arg.net_size = io_size * nreqs * METADATA;
                    arg.output = out_dir + template_fn + std::to_string(io_size * nreqs) + ".json";
                    templates.push_back(arg);
                }
            }
        }
	}

};

//Prototypes
bool LoadTemplate(std::string template_path, bool &is_template, LayerInfo *&layers, int &num_layers);
void ModifyTemplate(LayerInfo *layers, int num_layers, size_t net_size);
void SaveInstance(std::string output_path, LayerInfo *layers, int num_layers);

#endif
