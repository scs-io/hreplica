// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This is an example illustrating the use the general purpose non-linear
    least squares optimization routines from the dlib C++ Library.

    This example program will demonstrate how these routines can be used for data fitting.
    In particular, we will generate a set of data and then use the least squares
    routines to infer the parameters of the model which generated the data.
*/


#include <dlib/optimization.h>
#include <iostream>
#include <vector>


using namespace std;
using namespace dlib;

// ----------------------------------------------------------------------------------------

typedef matrix<double,1,1> input_vector;
typedef matrix<double,1,1> parameter_vector;

// ----------------------------------------------------------------------------------------

// We will use this function to generate data.  It represents a function of 2 variables
// and 3 parameters.   The least squares procedure will be used to infer the values of
// the 3 parameters based on a set of input/output pairs.
double model (
        const input_vector& input,
        const parameter_vector& params
)
{
    const double c1 = params(0);
    const double S = input(0);

    return c1*S;
}

// ----------------------------------------------------------------------------------------

// This function is the "residual" for a least squares problem.   It takes an input/output
// pair and compares it to the output of our model and returns the amount of error.  The idea
// is to find the set of parameters which makes the residual small on all the data pairs.
double residual (
        const std::pair<input_vector, double>& data,
        const parameter_vector& params
)
{
    return pow(model(data.first, params) - data.second, 2);
}

// ----------------------------------------------------------------------------------------

// This function is the derivative of the residual() function with respect to the parameters.
parameter_vector residual_derivative (
        const std::pair<input_vector, double>& data,
        const parameter_vector& params
)
{
    parameter_vector der;

    const double c1 = params(0);
    const double S = data.first(0);
    const double tc = data.second;

    der(0) = 2*(c1*S - tc);

    return der;
}

// ----------------------------------------------------------------------------------------

int main()
{
    // randomly pick a set of parameters to use in this example
    const parameter_vector params = 10*randm(1,1);
    cout << "params: " << trans(params) << endl;


    // Now let's generate a bunch of input/output pairs according to our model.
    std::vector<std::pair<input_vector, double> > data_samples;
    input_vector input;
    for (int i = 0; i < 1000; ++i)
    {
        input = 10*randm(1,1);
        const double output = model(input, params);

        // save the pair
        data_samples.push_back(make_pair(input, output));
    }

    // Now let's use the solve_least_squares_lm() routine to figure out what the
    // parameters are based on just the data_samples.
    parameter_vector x;
    x = 1;

    cout << "Use Levenberg-Marquardt" << endl;
    // Use the Levenberg-Marquardt method to determine the parameters which
    // minimize the sum of all squared residuals.
    solve_least_squares_lm(objective_delta_stop_strategy(1e-7).be_verbose(),
                           residual,
                           residual_derivative,
                           data_samples,
                           x);

    // Now x contains the solution.  If everything worked it will be equal to params.
    cout << "inferred parameters: "<< trans(x) << endl;
    cout << "solution error:      "<< length(x - params) << endl;
    cout << endl;








    cout << "Use Levenberg-Marquardt Der" << endl;
    // Use the Levenberg-Marquardt method to determine the parameters which
    // minimize the sum of all squared residuals.
    solve_least_squares_lm(objective_delta_stop_strategy(1e-7).be_verbose(),
                           residual,
                           derivative(residual),
                           data_samples,
                           x);

    // Now x contains the solution.  If everything worked it will be equal to params.
    cout << "inferred parameters: "<< trans(x) << endl;
    cout << "solution error:      "<< length(x - params) << endl;
    cout << endl;
}
