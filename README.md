# Introduction

HCompress is an adaptive compression algorithm that utilizes machine learning
to select different compression libraries depending on the workload, data type,
data format, and underlying hardware.

# Dependencies

HCompress is designed for Linux environments.

HCompress depends on the following libraries:
1. gcc 7.3+
2. [cmake-3.13.3+](https://cmake.org/download/)
3. [mpich](https://www.mpich.org/downloads/)
4. [HDF5](git clone -b develop --single-branch https://bitbucket.hdfgroup.org/scm/hdffv/hdf5.git)
4. [H5Part]
5. [boost_1_69_0+](https://www.boost.org/)
6. [rapidjson](https://github.com/Tencent/rapidjson)
7. TODO: rpclib

# Installation
## GCC
```sh
$ sudo apt install gcc
```

## CMake
1. Download and unzip the latest release from the cmake website.
2. Inside the directory where the zip file contents are extracted,
  ```sh
  $ ./configure
  $ make
  $ make install
  ```
3. Restart your terminal and check your cmake version with `$ cmake --version`.


## MPICH:
1. Download and unzip latest stable release from mpich website.
2. Inside the directory where zip file contents are extracted,
  ```sh
  $ ./configure --prefix=${HOME}/install --enable-fast=03 --enable-shared --enable-romio --enable-threads --disable-fortran --disable-fc
  $ make && make install
  ```

## HDF5:

git clone -b develop --single-branch https://bitbucket.hdfgroup.org/scm/hdffv/hdf5.git
cd hdf5
vi hl/src/H5LT.c
line 891 add 
hid_t vol_id = H5VLget_connector_id(H5VL_NATIVE_NAME);
H5Pset_vol(fapl, vol_id, NULL);
mkdir build
cd build



cmake -DCMAKE_INSTALL_PREFIX=/home/hariharan/software/install -DHDF5_ENABLE_DIRECT_VFD=ON -DHDF5_BUILD_CPP_LIB=OFF -DHDF5_ENABLE_PARALLEL=ON ../



make -j8 && make install




RapidJSON

git clone https://github.com/Tencent/rapidjson.git
cd rapidjson
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/home/hariharan/software/install ../
make -j8 && make install

H5Part

git clone -b 1.6 --single-branch https://gitlab.psi.ch/H5hut/src.git H5hut
cd H5hut
./autogen.sh
./configure --prefix=/home/hariharan/software/install --with-hdf5=/home/hariharan/software/install --with-mpi=/home/hariharan/software/install --enable-shared --enable-parallel

vi src/H5Part.c
line:2138 else if ( H5Oget_info ( obj_id, &objinfo,0 ) < 0 ) {
line 2088  if( H5Oget_info_by_name( group_id, member_name, &objinfo,0, H5P_DEFAULT ) < 0 ) {
line 3764 if (H5Pset_fapl_mpio( f->access_prop, comm, 0 ) < 0) {
line 261 add lines 
MPI_Info_create(&info) ;
MPI_Info_set(info, "direct_read", "true");
MPI_Info_set(info, "direct_write", "true");
 
vi test/testframe.c
line 670 H5Oget_info(list[i], &info,0);

make
make install

Dlib
git clone https://github.com/davisking/dlib
cd dlib
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/home/hariharan/software/install ../
make -j8 && make install

openblas

git clone https://github.com/xianyi/OpenBLAS
cd OpenBLAS
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/home/hariharan/software/install ../
make -j8 && make install


